
void setup(){
  int startTime = millis();
  println("Started");
  int prevSeconds = -1;
  int currSeconds;
  while(abs(millis()-startTime) < 10000){
    currSeconds = int(abs(millis()-startTime)/1000);
    if(abs(millis()-startTime) % 1000 == 0 && prevSeconds != currSeconds)
      println(15 - currSeconds + " seconds left");
    prevSeconds = currSeconds;
  }
  println("Done");
}
