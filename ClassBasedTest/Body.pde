import processing.serial.*;
import toxi.geom.*;
import toxi.processing.*;

public class Body{
  HashMap<String, LimbSegment> segList = new HashMap<String, LimbSegment>();// creating a hashmap to store all of the limb segments
  
  Vec3D rightPivotPointDef;
  Vec3D leftPivotPointDef;
  Vec3D rightPtQuat;// this is the default pivot point with the sensor data applied to it
  Vec3D leftPtQuat;
  Vec3D rightPtCal;// this is the pivot point with the calibration rotation applied to it as well
  Vec3D leftPtCal;
  
  
  public Body(){// public Body()    
    segList.put("torso", new LimbSegment("torso"));
    segList.get("torso").setDimensions(400,16,200);
    segList.put("rightBicep", new LimbSegment("right arm: bicep"));
    segList.get("rightBicep").setDimensions(200,20,30);
    segList.get("rightBicep").setColor(color(0,0,255,230)); //<>//
    segList.put("rightForearm", new LimbSegment("right arm: forearm"));
    segList.get("rightForearm").setDimensions(180,16,20); //<>//
    segList.put("leftBicep", new LimbSegment("left arm: bicep"));
    segList.get("leftBicep").setDimensions(200,20,30);
    segList.put("leftForearm", new LimbSegment("left arm: forearm"));
    segList.get("leftForearm").setDimensions(180,16,20);
    rightPivotPointDef = new Vec3D(segList.get("torso").w, segList.get("torso").d/2, 0);
    leftPivotPointDef = new Vec3D(segList.get("torso").w, -segList.get("torso").d/2, 0);
    calculateVals();
  }// public Body()
  
  
  
  public void calibrateAll(){
    int startTime = millis();
    println("Starting calibrtion. Hold a T-pose for 10 seconds");
    while(abs(millis()-startTime) < 1000){
      // Do nothing while waiting
      // maybe print a countdown?
    }
    println("Calibrating now");
    for (LimbSegment limb : segList.values())
      if (limb.enabled)
        limb.calibrate();
  }
  
  public void calculateVals(){// public void calculateVals()
    for (LimbSegment limb : segList.values())
      if (limb.enabled)
        limb.calculate();
    
    rightPtQuat = segList.get("torso").quatMat.applyTo(rightPivotPointDef);
    rightPtCal = segList.get("torso").quatReturnerMat.applyTo(rightPtQuat);
    leftPtQuat = segList.get("torso").quatMat.applyTo(leftPivotPointDef);
    leftPtCal = segList.get("torso").quatReturnerMat.applyTo(leftPtQuat);
    segList.get("rightBicep").pivotPoint.setX(segList.get("torso").pivotPoint.x + rightPtQuat.x);
    segList.get("rightBicep").pivotPoint.setY(segList.get("torso").pivotPoint.y - rightPtQuat.z);
    segList.get("rightBicep").pivotPoint.setZ(segList.get("torso").pivotPoint.z - rightPtQuat.y);
    segList.get("leftBicep").pivotPoint.setX(segList.get("torso").pivotPoint.x + leftPtQuat.x);
    segList.get("leftBicep").pivotPoint.setY(segList.get("torso").pivotPoint.y - leftPtQuat.z);
    segList.get("leftBicep").pivotPoint.setZ(segList.get("torso").pivotPoint.z - leftPtQuat.y);
    
    segList.get("rightForearm").pivotPoint.setX(segList.get("rightBicep").pivotPoint.x+(2*segList.get("rightBicep").calibratedCompVector.x));// encapsulate this in LimbSegment- define a parent LimbSegment for each LimbSegment
    segList.get("rightForearm").pivotPoint.setY(segList.get("rightBicep").pivotPoint.y-(2*segList.get("rightBicep").calibratedCompVector.z));// then just have a few exceptions for the pivot points coming off of the torso
    segList.get("rightForearm").pivotPoint.setZ(segList.get("rightBicep").pivotPoint.z-(2*segList.get("rightBicep").calibratedCompVector.y));// other than that, this method of calculating pivot points should work for everthing
    segList.get("leftForearm").pivotPoint.setX(segList.get("leftBicep").pivotPoint.x+(2*segList.get("leftBicep").calibratedCompVector.x));// else that just pivots about the end of another limb
    segList.get("leftForearm").pivotPoint.setY(segList.get("leftBicep").pivotPoint.y-(2*segList.get("leftBicep").calibratedCompVector.z));
    segList.get("leftForearm").pivotPoint.setZ(segList.get("leftBicep").pivotPoint.z-(2*segList.get("leftBicep").calibratedCompVector.y));
    
    for (LimbSegment limb : segList.values())
      if (limb.enabled)
        limb.calculateTranslateVector();
  }// public void calculateVals()
  
  public void display(){// public void display()
    body.calculateVals();
  
    for (LimbSegment currLimb : segList.values()){
      if (currLimb.enabled){
        pushMatrix();// push 1
        translate(currLimb.translateVector.x, currLimb.translateVector.y, currLimb.translateVector.z);
        rotate(currLimb.quatReturnerAxis[0], -currLimb.quatReturnerAxis[1], currLimb.quatReturnerAxis[3], currLimb.quatReturnerAxis[2]);
        rotate(currLimb.quatAxis[0], -currLimb.quatAxis[1], currLimb.quatAxis[3], currLimb.quatAxis[2]);
        fill(currLimb.col);
        box(currLimb.w, currLimb.h, currLimb.d);
        popMatrix();// pop 1 
      }// if (currLimb.enabled)
    }// for (LimbSegment currLimb : segList.values())
    
  }// public void display()
  
  public void writePorts(int numTimes){// public void writePorts(int numTimes)
    for (LimbSegment currLimb : segList.values())
      if (currLimb.enabled)
        for (int i = 0; i < numTimes; ++i)
          currLimb.port.write('r');
  }// public void writePorts(int numTimes)
  
}// public class Body
