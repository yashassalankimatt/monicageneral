import processing.serial.*;

Serial mySerial;

String rawString = null;
int newLine = 10;
double rotX;
double rotY;
double rotZ;
double gForceX;
double gForceY;
double gForceZ;

void setup() {
  size(800,400,P2D);
  pixelDensity(2);
  printArray(Serial.list());
  String myPort = Serial.list() [0];
  mySerial = new Serial(this, myPort, 115200);
}

void draw() {
  mySerial.write('r');
  while(mySerial.available() > 0){
    rawString = mySerial.readStringUntil(newLine);
    if(rawString != null){
      String [] tokens = rawString.split(" ");
      rotX = Double.parseDouble(tokens[0]);
      rotY = Double.parseDouble(tokens[1]);
      rotZ = Double.parseDouble(tokens[2]);
      gForceX = Double.parseDouble(tokens[3]);
      gForceY = Double.parseDouble(tokens[4]);
      gForceZ = Double.parseDouble(tokens[5]);
    }
    printValues();
    //now on to the actual graphics
    background(50);
    stroke(255);
    strokeWeight(7);
    fill(150);
    float x = map((float)gForceZ, -1.5, 1.5, 0, 400);
    float y = map((float)gForceY, 1.5, -1.5, 0, 400);
    float z = map((float)gForceX, -1.5, 1.5, 400, 800);
    line(400,0,400,400);
    line(200, 200, x, y);
    line(600, 200, z, y);
    //rect(200,200,150,10);
    //rotate(PI/36);
  }
}

void setValues(){
  while(mySerial.available() > 0){
    rawString = mySerial.readStringUntil(newLine);
    if(rawString != null){
      String [] tokens = rawString.split(" ");
      rotX = Double.parseDouble(tokens[0]);
      rotY = Double.parseDouble(tokens[1]);
      rotZ = Double.parseDouble(tokens[2]);
      gForceX = Double.parseDouble(tokens[3]);
      gForceY = Double.parseDouble(tokens[4]);
      gForceZ = Double.parseDouble(tokens[5]);
    }
  }
}

void printValues(){
    System.out.print("Gyro (deg)");
    System.out.print(" X=");
    System.out.print(rotX);
    System.out.print(" Y=");
    System.out.print(rotY);
    System.out.print(" Z=");
    System.out.print(rotZ);
    System.out.print(" Accel (g)");
    System.out.print(" X=");
    System.out.print(gForceX);
    System.out.print(" Y=");
    System.out.print(gForceY);
    System.out.print(" Z=");
    System.out.println(gForceZ);
}
