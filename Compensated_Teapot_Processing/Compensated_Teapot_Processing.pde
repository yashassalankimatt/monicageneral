import processing.serial.*;
import processing.opengl.*;
import toxi.geom.*;
import toxi.processing.*;

ToxiclibsSupport gfx;

Serial port;
char[] teapotPacket = new char[14];
int serialCount = 0;
int synced = 0;
int interval = 0;

float[] q = new float[4];

int width1 = 200;
int height1 = 10;
int depth1 = 30;

Quaternion quat = new Quaternion(1,0,0,0);
float axis[];
Matrix4x4 mat;
Vec3D v = new Vec3D(width1/2, 0, 0);
Vec3D v2 = new Vec3D();

void setup(){
  size(600, 600, P3D);
  gfx = new ToxiclibsSupport(this);
  lights();
  smooth();
  println(Serial.list());
  String portName = Serial.list()[2];
  port = new Serial(this, portName, 500000);
  
  port.write('r');
  port.write('r');
  port.write('r');
  port.write('r');
}

void draw(){
  if (millis() - interval > 1000){
    port.write('r');
    interval = millis();
  }
  lights();
  smooth();
  background(0);
  
  axis = quat.toAxisAngle();
  mat = quat.toMatrix4x4();
  v2 = mat.applyTo(v);
  pushMatrix();
  //translate(width/2, height/2); //use for stationary movement about center of mass
  translate((width/2)+v2.x, (height/2)-v2.z, -v2.y);
  rotate(axis[0], -axis[1], axis[3], axis[2]);
  fill(255,0,0,230);
  box(width1, height1, depth1);
  popMatrix();
}

void serialEvent(Serial port){
  interval = millis();
  while (port.available() > 0) {
        int ch = port.read();

        if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
        synced = 1;
        print ((char)ch);

        if ((serialCount == 1 && ch != 2)
            || (serialCount == 12 && ch != '\r')
            || (serialCount == 13 && ch != '\n'))  {
            serialCount = 0;
            synced = 0;
            return;
        }

        if (serialCount > 0 || ch == '$') {
            teapotPacket[serialCount++] = (char)ch;
            if (serialCount == 14) {
                serialCount = 0; // restart packet byte position
                
                // get quaternion from data packet
                q[0] = ((teapotPacket[2] << 8) | teapotPacket[3]) / 16384.0f;
                q[1] = ((teapotPacket[4] << 8) | teapotPacket[5]) / 16384.0f;
                q[2] = ((teapotPacket[6] << 8) | teapotPacket[7]) / 16384.0f;
                q[3] = ((teapotPacket[8] << 8) | teapotPacket[9]) / 16384.0f;
                for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                
                // set our toxilibs quaternion to new data
                quat.set(q[0], q[1], q[2], q[3]);
            }
        }
  }
}
