import processing.serial.*;//importing the Serial library to establish communications with the Arduinos
import toxi.geom.*;//importing the toxicLibs geometry library
import toxi.processing.*;//importing the toxiclibs processing library

ToxiclibsSupport gfx;//creating an instance of the toxiclibs support object

float startTime = 0;//this denotes when the program has received a serialEvent and when it starts recording
boolean recorded = false;//denotes if it is time for the calibration inverse quats to be recorded
int serialIteration = 0;//denotes the number of iterations of the serialEvent port

//this section initializes the Serial ports for each of the Arduino-sensor systems
Serial portRightBicep;
Serial portRightForearm;
Serial portLeftBicep;
Serial portLeftForearm;
Serial portTorso;
Serial portRightThigh;
Serial portRightCalf;
Serial portLeftThigh;
Serial portLeftCalf;

//this section is for the variables used in the recording of the data from the Serial ports
char[] dataPacket = new char[14];//contains the pure byte data from the serial port
int serialCount = 0;//keeps track of how many bytes have been recorded
int synced = 0;//keeps track if the transmission is in sync with the recording on the processing sketch
int interval = 0;//used to write to serial port again if transmission is detected as having stopped
int sensorNum = 1;//not used now, but was used to keep track of sensors when more than one sensor was connected to one Arduino
float[] q = new float[4];//quaternion float packet from byte data
float[] q2 = new float[4];//second quaternion float packet from byte data, used previously when more than one sensor was connected to one Arduino

//dimensions of the boxes
int widthRBicep = 100;
int heightRBicep = 10;
int depthRBicep = 15;

int widthRForearm = 90;
int heightRForearm = 8;
int depthRForearm = 10;

int widthLBicep = 100;
int heightLBicep = 10;
int depthLBicep = 15;

int widthLForearm = 90;
int heightLForearm = 8;
int depthLForearm = 10;

int widthTorso = 200;
int heightTorso = 8;
int depthTorso = 100;

int widthRThigh = 100;
int heightRThigh = 10;
int depthRThigh = 15;

int widthLThigh = 100;
int heightLThigh = 10;
int depthLThigh = 15;

int widthRCalf = 90;
int heightRCalf = 8;
int depthRCalf = 10;

int widthLCalf = 90;
int heightLCalf = 8;
int depthLCalf = 10;

Quaternion quatA = new Quaternion(1,0,0,0);
float axisA[];
Matrix4x4 matA;
Vec3D vA = new Vec3D(widthRBicep/2, 0, 0);
Vec3D vA2 = new Vec3D();
Quaternion quatAReturner = new Quaternion(1,0,0,0);
float AReturnerAxis[];
Matrix4x4 AReturnerMatrix;
Vec3D vA3 = new Vec3D();

Quaternion quatB = new Quaternion(1,0,0,0);
float axisB[];
Matrix4x4 matB;
Vec3D vB = new Vec3D(widthRForearm/2, 0, 0);
Vec3D vB2 = new Vec3D();
Quaternion quatBReturner = new Quaternion(1,0,0,0);
float BReturnerAxis[];
Matrix4x4 BReturnerMatrix;
Vec3D vB3 = new Vec3D();

Quaternion quatC = new Quaternion(1,0,0,0);
float axisC[];
Matrix4x4 matC;
Vec3D vC = new Vec3D(widthLBicep/2, 0, 0);
Vec3D vC2 = new Vec3D();
Quaternion quatCReturner = new Quaternion(1,0,0,0);
float CReturnerAxis[];
Matrix4x4 CReturnerMatrix;
Vec3D vC3 = new Vec3D();

Quaternion quatLeftDefault = new Quaternion(0,0,0,1);

Quaternion quatD = new Quaternion(1,0,0,0);
float axisD[];
Matrix4x4 matD;
Vec3D vD = new Vec3D(widthLForearm/2, 0, 0);
Vec3D vD2 = new Vec3D();
Quaternion quatDReturner = new Quaternion(1,0,0,0);
float DReturnerAxis[];
Matrix4x4 DReturnerMatrix;
Vec3D vD3 = new Vec3D();

Quaternion quatE = new Quaternion(1,0,0,0);
float axisE[];
Matrix4x4 matE;
Vec3D vE = new Vec3D(widthTorso/2, 0, 0);
Vec3D vE2 = new Vec3D();
Vec3D vE3 = new Vec3D();
Vec3D vE4 = new Vec3D();
Vec3D vE5 = new Vec3D();
Vec3D vELeft1 = new Vec3D(widthTorso, -depthTorso/2, 0);
Vec3D vERight1 = new Vec3D(widthTorso, depthTorso/2, 0);
Vec3D vELeft2 = new Vec3D();
Vec3D vERight2 = new Vec3D();
Vec3D vELeft3 = new Vec3D();
Vec3D vERight3 = new Vec3D();
Vec3D vELeft4 = new Vec3D();
Vec3D vERight4 = new Vec3D();
Vec3D vELeft5 = new Vec3D();
Vec3D vERight5 = new Vec3D();
Vec3D vELL1 = new Vec3D(0,-depthTorso/2,0);
Vec3D vERL1 = new Vec3D(0,depthTorso/2,0);
Vec3D vELL2 = new Vec3D();
Vec3D vERL2 = new Vec3D();
Vec3D vELL3 = new Vec3D();
Vec3D vERL3 = new Vec3D();
Vec3D vELL4 = new Vec3D();
Vec3D vERL4 = new Vec3D();
Vec3D vELL5 = new Vec3D();
Vec3D vERL5 = new Vec3D();
Vec3D vETorso1 = new Vec3D(widthTorso, 0, 0);
Vec3D vETorso2 = new Vec3D();
Vec3D vETorso3 = new Vec3D();
Vec3D vETorso4 = new Vec3D();
Vec3D vETorso5 = new Vec3D();
Quaternion qE1 = new Quaternion(1,0,0,0);
Quaternion qE2 = new Quaternion(1,0,0,0);
Quaternion qE3 = new Quaternion(1,0,0,0);

Quaternion quatF = new Quaternion(1,0,0,0);
float axisF[];
Matrix4x4 matF;
Vec3D vF = new Vec3D(widthRThigh/2, 0, 0);
Vec3D vF2 = new Vec3D();
Vec3D vF3 = new Vec3D();
Vec3D vF4 = new Vec3D();
Vec3D vF5 = new Vec3D();
Quaternion qF1 = new Quaternion(1,0,0,0);
Quaternion qF2 = new Quaternion(1,0,0,0);
Quaternion qF3 = new Quaternion(1,0,0,0);

Quaternion quatG = new Quaternion(1,0,0,0);
float axisG[];
Matrix4x4 matG;
Vec3D vG = new Vec3D(widthRCalf/2, 0, 0);
Vec3D vG2 = new Vec3D();
Vec3D vG3 = new Vec3D();
Vec3D vG4 = new Vec3D();
Vec3D vG5 = new Vec3D();
Quaternion qG1 = new Quaternion(1,0,0,0);
Quaternion qG2 = new Quaternion(1,0,0,0);
Quaternion qG3 = new Quaternion(1,0,0,0);

Quaternion quatH = new Quaternion(1,0,0,0);
float axisH[];
Matrix4x4 matH;
Vec3D vH = new Vec3D(widthLThigh/2, 0, 0);
Vec3D vH2 = new Vec3D();
Vec3D vH3 = new Vec3D();
Vec3D vH4 = new Vec3D();
Vec3D vH5 = new Vec3D();
Quaternion qH1 = new Quaternion(1,0,0,0);
Quaternion qH2 = new Quaternion(1,0,0,0);
Quaternion qH3 = new Quaternion(1,0,0,0);

Quaternion quatI = new Quaternion(1,0,0,0);
float axisI[];
Matrix4x4 matI;
Vec3D vI= new Vec3D(widthLCalf/2, 0, 0);
Vec3D vI2 = new Vec3D();
Vec3D vI3 = new Vec3D();
Vec3D vI4 = new Vec3D();
Vec3D vI5 = new Vec3D();
Quaternion qI1 = new Quaternion(1,0,0,0);
Quaternion qI2 = new Quaternion(1,0,0,0);
Quaternion qI3 = new Quaternion(1,0,0,0);

int segEdit = 1;//1= torso; 2= right thigh; 3= left thigh; 4= right calf; 5= left calf

int t1E = 0;//torso
int t2E = 0;
int t3E = 0;

int t1F = 0;//right thigh
int t2F = 0;
int t3F = 0;

int t1G = 0;//right calf
int t2G = 0;
int t3G = 0;

int t1H = 0;//left thigh
int t2H = 0;
int t3H = 0;

int t1I = 0;//left calf
int t2I = 0;
int t3I = 0;

float rotYDegrees = 0;
Quaternion rotYQuat = new Quaternion();
float[] rotYAxis;
Matrix4x4 rotYMat;
Vec3D viewTranslator1 = new Vec3D();
Vec3D viewTranslator2 = new Vec3D();

Vec3D pivotPointTorso = new Vec3D();
Vec3D pivotPointRightArm = new Vec3D();
Vec3D pivotPointLeftArm = new Vec3D();
Vec3D pivotPointRightLeg = new Vec3D();
Vec3D pivotPointLeftLeg = new Vec3D();

boolean calibBool = false;

void setup(){
  size(1000, 800, P3D);
  gfx = new ToxiclibsSupport(this);
  lights();
  smooth();
  println(Serial.list());
  portRightBicep = new Serial(this, "COM17", 500000);
  portRightForearm = new Serial(this, "COM16", 500000);
  portLeftBicep = new Serial(this, "COM15", 500000);
  portLeftForearm = new Serial(this, "COM14", 500000);
  portTorso = new Serial(this,"COM26", 500000);
  portRightThigh = new Serial(this,"COM24", 500000);
  portRightCalf = new Serial(this,"COM23", 500000);
  portLeftThigh =  new Serial(this,"COM22", 500000);
  portLeftCalf = new Serial(this,"COM21", 500000);
  
  
  pivotPointTorso = new Vec3D((width/2), (height/2), 0);
  viewTranslator1 = new Vec3D(pivotPointTorso.x, 0, 0);
  
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portTorso.write('r');
  portTorso.write('r');
  portTorso.write('r');
  portTorso.write('r');
  portRightThigh.write('r');
  portRightThigh.write('r');
  portRightThigh.write('r');
  portRightThigh.write('r');
  portRightCalf.write('r');
  portRightCalf.write('r');
  portRightCalf.write('r');
  portRightCalf.write('r');
  portLeftThigh.write('r');
  portLeftThigh.write('r');
  portLeftThigh.write('r');
  portLeftThigh.write('r');
  portLeftCalf.write('r');
  portLeftCalf.write('r');
  portLeftCalf.write('r');
  portLeftCalf.write('r');
}

void draw(){
  portRightBicep.write('r');
  portRightForearm.write('r');
  portLeftBicep.write('r');
  portLeftForearm.write('r');
  portTorso.write('r');
  portRightThigh.write('r');
  portRightCalf.write('r');
  portLeftThigh.write('r');
  portLeftCalf.write('r');
  if (millis() - interval > 1000){
    portRightBicep.write('r');
    portRightForearm.write('r');
    portLeftBicep.write('r');
    portLeftForearm.write('r');
    portTorso.write('r');
    portRightThigh.write('r');
    portRightCalf.write('r');
    portLeftThigh.write('r');
    portLeftCalf.write('r');
    interval = millis();
    serialIteration = 0;
    recorded = false;
  }
  
  lights();
  smooth();
  background(0);
  
  if(calibBool){//if stableState has been reached and inverse quaternions have not been recorded
    quatAReturner = quatA.getConjugate();//gets the motion to return offset quaternion back to default position
    quatBReturner = quatB.getConjugate();
    quatCReturner = (quatC.getConjugate()).multiply(quatLeftDefault);//gets motion to return quaternion to quatLeftDefault
    quatDReturner = (quatD.getConjugate()).multiply(quatLeftDefault);
    //recorded = true;
    calibBool = false;
    println("Steady state reached and inverse quats recorded!");
  }
  pushMatrix();
  rotYQuat = Quaternion.createFromAxisAngle(new Vec3D(0,0,1),radians(rotYDegrees));
  rotYAxis = rotYQuat.toAxisAngle();
  rotYMat = rotYQuat.toMatrix4x4();
  viewTranslator2 = rotYMat.applyTo(viewTranslator1);
  translate(600-viewTranslator2.x,0,viewTranslator2.y);
  rotate(rotYAxis[0], -rotYAxis[1], rotYAxis[3], rotYAxis[2]);
  //println(viewTranslator2);
  
  
  AReturnerAxis = quatAReturner.toAxisAngle();//converting to axisAngles to use the rotate function
  AReturnerMatrix = quatAReturner.toMatrix4x4();//converting to matrix in order to operate on vectors
  axisA = quatA.toAxisAngle();//converting to axisAngles to use the rotate function
  matA = quatA.toMatrix4x4();//converting to matrix in order to opertate on vectors
  vA2 = matA.applyTo(vA);//applies the actual sensor data rotation to a starting position vector
  vA3 = AReturnerMatrix.applyTo(vA2);//applies the calibration rotation to the sensor rotation
  pushMatrix();//pushes a matrix onto coordinate plane to individually manipulate this shape
  translate(pivotPointRightArm.x+vA3.x, pivotPointRightArm.y-vA3.z, pivotPointRightArm.z-vA3.y);//translates coordinate plane
  rotate(AReturnerAxis[0], -AReturnerAxis[1], AReturnerAxis[3], AReturnerAxis[2]);//rotates on calibration data
  rotate(axisA[0], -axisA[1], axisA[3], axisA[2]);//rotation based on sensor data
  fill(255,0,0,230);//defining the color and transparency
  box(widthRBicep, heightRBicep, depthRBicep);//creating a box with predefined dimensions
  popMatrix();//popping out of matrix to manipulate other object
  
  BReturnerAxis = quatBReturner.toAxisAngle();
  BReturnerMatrix = quatBReturner.toMatrix4x4();
  axisB = quatB.toAxisAngle();
  matB = quatB.toMatrix4x4();
  vB2 = matB.applyTo(vB);
  vB3 = BReturnerMatrix.applyTo(vB2);
  pushMatrix();
  translate(pivotPointRightArm.x+(2*vA3.x)+vB3.x, pivotPointRightArm.y-(2*vA3.z)-vB3.z, pivotPointRightArm.z+(-2*vA3.y)-vB3.y);
  rotate(BReturnerAxis[0], -BReturnerAxis[1], BReturnerAxis[3], BReturnerAxis[2]);
  rotate(axisB[0], -axisB[1], axisB[3], axisB[2]);
  fill(255,0,0,230);
  box(widthRForearm, heightRForearm, depthRForearm);
  popMatrix();
  
  CReturnerAxis = quatCReturner.toAxisAngle();
  CReturnerMatrix = quatCReturner.toMatrix4x4();
  axisC = quatC.toAxisAngle();
  matC = quatC.toMatrix4x4();
  vC2 = matC.applyTo(vC);
  vC3 = CReturnerMatrix.applyTo(vC2);
  pushMatrix();
  translate(pivotPointLeftArm.x+vC3.x, pivotPointLeftArm.y-vC3.z, pivotPointLeftArm.z-vC3.y);
  rotate(CReturnerAxis[0], -CReturnerAxis[1], CReturnerAxis[3], CReturnerAxis[2]);
  rotate(axisC[0], -axisC[1], axisC[3], axisC[2]);
  fill(255,0,0,230);
  box(widthLBicep, heightLBicep, depthLBicep);
  popMatrix();
  
  DReturnerAxis = quatDReturner.toAxisAngle();
  DReturnerMatrix = quatDReturner.toMatrix4x4();
  axisD = quatD.toAxisAngle();
  matD = quatD.toMatrix4x4();
  vD2 = matD.applyTo(vD);
  vD3 = DReturnerMatrix.applyTo(vD2);
  pushMatrix();
  translate(pivotPointLeftArm.x+(2*vC3.x)+vD3.x, pivotPointLeftArm.y-(2*vC3.z)-vD3.z, pivotPointLeftArm.z+(-2*vC3.y)-vD3.y);
  rotate(DReturnerAxis[0], -DReturnerAxis[1], DReturnerAxis[3], DReturnerAxis[2]);
  rotate(axisD[0], -axisD[1], axisD[3], axisD[2]);
  fill(255,0,0,230);
  box(widthLForearm, heightLForearm, depthLForearm);
  popMatrix();
  
  qE1 = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(t1E));
  qE2 = Quaternion.createFromAxisAngle(new Vec3D(0,1,0), radians(t2E));
  qE3 = Quaternion.createFromAxisAngle(new Vec3D(1,0,0), radians(t3E));
  axisE = quatE.toAxisAngle();
  matE = quatE.toMatrix4x4();
  vE2 = matE.applyTo(vE);
  vE3 = (qE1.toMatrix4x4()).applyTo(vE2);
  vE4 = (qE2.toMatrix4x4()).applyTo(vE3);
  vE5 = (qE3.toMatrix4x4()).applyTo(vE4);
  vELeft2 = matE.applyTo(vELeft1);
  vELeft3 = (qE1.toMatrix4x4()).applyTo(vELeft2);
  vELeft4 = (qE2.toMatrix4x4()).applyTo(vELeft3);
  vELeft5 = (qE3.toMatrix4x4()).applyTo(vELeft4);
  vERight2 = matE.applyTo(vERight1);
  vERight3 = (qE1.toMatrix4x4()).applyTo(vERight2);
  vERight4 = (qE2.toMatrix4x4()).applyTo(vERight3);
  vERight5 = (qE3.toMatrix4x4()).applyTo(vERight4);
  vELL2 = matE.applyTo(vELL1);
  vELL3 = (qE1.toMatrix4x4()).applyTo(vELL2);
  vELL4 = (qE2.toMatrix4x4()).applyTo(vELL3);
  vELL5 = (qE3.toMatrix4x4()).applyTo(vELL4);
  vERL2 = matE.applyTo(vERL1);
  vERL3 = (qE1.toMatrix4x4()).applyTo(vERL2);
  vERL4 = (qE2.toMatrix4x4()).applyTo(vERL3);
  vERL5 = (qE3.toMatrix4x4()).applyTo(vERL4);
  vETorso2 = matE.applyTo(vETorso1);
  vETorso3 = (qE1.toMatrix4x4()).applyTo(vETorso2);
  vETorso4 = (qE2.toMatrix4x4()).applyTo(vETorso3);
  vETorso5 = (qE3.toMatrix4x4()).applyTo(vETorso4);
  pivotPointRightArm.setX(pivotPointTorso.x+vERight5.x);
  pivotPointRightArm.setY(pivotPointTorso.y-vERight5.z);
  pivotPointRightArm.setZ(pivotPointTorso.z-vERight5.y);
  pivotPointLeftArm.setX(pivotPointTorso.x+vELeft5.x);
  pivotPointLeftArm.setY(pivotPointTorso.y-vELeft5.z);
  pivotPointLeftArm.setZ(pivotPointTorso.z-vELeft5.y);
  pivotPointRightLeg.setX(pivotPointTorso.x+vERL5.x);
  pivotPointRightLeg.setY(pivotPointTorso.y-vERL5.z);
  pivotPointRightLeg.setZ(pivotPointTorso.z-vERL5.y);
  pivotPointLeftLeg.setX(pivotPointTorso.x+vELL5.x);
  pivotPointLeftLeg.setY(pivotPointTorso.y-vELL5.z);
  pivotPointLeftLeg.setZ(pivotPointTorso.z-vELL5.y);
  pushMatrix();
  translate(pivotPointTorso.x+vE5.x, pivotPointTorso.y-vE5.z, pivotPointTorso.z-vE5.y);
  rotate(qE1.toAxisAngle()[0], -qE1.toAxisAngle()[1], qE1.toAxisAngle()[3], qE1.toAxisAngle()[2]);
  rotate(qE2.toAxisAngle()[0], -qE2.toAxisAngle()[1], qE2.toAxisAngle()[3], qE2.toAxisAngle()[2]);
  rotate(qE3.toAxisAngle()[0], -qE3.toAxisAngle()[1], qE3.toAxisAngle()[3], qE3.toAxisAngle()[2]);
  rotate(axisE[0], -axisE[1], axisE[3], axisE[2]);
  fill(255,0,0,230);
  box(widthTorso, heightTorso, depthTorso);
  popMatrix();
  
  qF1 = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(t1F));
  qF2 = Quaternion.createFromAxisAngle(new Vec3D(0,1,0), radians(t2F));
  qF3 = Quaternion.createFromAxisAngle(new Vec3D(1,0,0), radians(t3F));
  axisF = quatF.toAxisAngle();
  matF = quatF.toMatrix4x4();
  vF2 = matF.applyTo(vF);
  vF3 = (qF1.toMatrix4x4()).applyTo(vF2);
  vF4 = (qF2.toMatrix4x4()).applyTo(vF3);
  vF5 = (qF3.toMatrix4x4()).applyTo(vF4);
  pushMatrix();
  translate(pivotPointRightLeg.x+vF5.x, pivotPointRightLeg.y-vF5.z, pivotPointRightLeg.z-vF5.y);
  rotate(qF1.toAxisAngle()[0], -qF1.toAxisAngle()[1], qF1.toAxisAngle()[3], qF1.toAxisAngle()[2]);
  rotate(qF2.toAxisAngle()[0], -qF2.toAxisAngle()[1], qF2.toAxisAngle()[3], qF2.toAxisAngle()[2]);
  rotate(qF3.toAxisAngle()[0], -qF3.toAxisAngle()[1], qF3.toAxisAngle()[3], qF3.toAxisAngle()[2]);
  rotate(axisF[0], -axisF[1], axisF[3], axisF[2]);
  fill(255,0,0,230);
  box(widthRThigh, heightRThigh, depthRThigh);
  popMatrix();
  
  qG1 = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(t1G));
  qG2 = Quaternion.createFromAxisAngle(new Vec3D(0,1,0), radians(t2G));
  qG3 = Quaternion.createFromAxisAngle(new Vec3D(1,0,0), radians(t3G));
  axisG = quatG.toAxisAngle();
  matG = quatG.toMatrix4x4();
  vG2 = matG.applyTo(vG);
  vG3 = (qG1.toMatrix4x4()).applyTo(vG2);
  vG4 = (qG2.toMatrix4x4()).applyTo(vG3);
  vG5 = (qG3.toMatrix4x4()).applyTo(vG4);
  pushMatrix();
  translate(pivotPointRightLeg.x+(2*vF5.x)+vG5.x, pivotPointRightLeg.y-(2*vF5.z)-vG5.z, pivotPointRightLeg.z-(2*vF5.y)-vG5.y);
  rotate(qG1.toAxisAngle()[0], -qG1.toAxisAngle()[1], qG1.toAxisAngle()[3], qG1.toAxisAngle()[2]);
  rotate(qG2.toAxisAngle()[0], -qG2.toAxisAngle()[1], qG2.toAxisAngle()[3], qG2.toAxisAngle()[2]);
  rotate(qG3.toAxisAngle()[0], -qG3.toAxisAngle()[1], qG3.toAxisAngle()[3], qG3.toAxisAngle()[2]);
  rotate(axisG[0], -axisG[1], axisG[3], axisG[2]);
  fill(255,0,0,230);
  box(widthRCalf, heightRCalf, depthRCalf);
  popMatrix();
  
  qH1 = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(t1H));
  qH2 = Quaternion.createFromAxisAngle(new Vec3D(0,1,0), radians(t2H));
  qH3 = Quaternion.createFromAxisAngle(new Vec3D(1,0,0), radians(t3H));
  axisH = quatH.toAxisAngle();
  matH = quatH.toMatrix4x4();
  vH2 = matH.applyTo(vH);
  vH3 = (qH1.toMatrix4x4()).applyTo(vH2);
  vH4 = (qH2.toMatrix4x4()).applyTo(vH3);
  vH5 = (qH3.toMatrix4x4()).applyTo(vH4);
  pushMatrix();
  translate(pivotPointLeftLeg.x+vH5.x, pivotPointLeftLeg.y-vH5.z, pivotPointLeftLeg.z-vH5.y);
  rotate(qH1.toAxisAngle()[0], -qH1.toAxisAngle()[1], qH1.toAxisAngle()[3], qH1.toAxisAngle()[2]);
  rotate(qH2.toAxisAngle()[0], -qH2.toAxisAngle()[1], qH2.toAxisAngle()[3], qH2.toAxisAngle()[2]);
  rotate(qH3.toAxisAngle()[0], -qH3.toAxisAngle()[1], qH3.toAxisAngle()[3], qH3.toAxisAngle()[2]);
  rotate(axisH[0], -axisH[1], axisH[3], axisH[2]);
  fill(255,0,0,230);
  box(widthLThigh, heightLThigh, depthLThigh);
  popMatrix();
  
  qI1 = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(t1I));
  qI2 = Quaternion.createFromAxisAngle(new Vec3D(0,1,0), radians(t2I));
  qI3 = Quaternion.createFromAxisAngle(new Vec3D(1,0,0), radians(t3I));
  axisI = quatI.toAxisAngle();
  matI = quatI.toMatrix4x4();
  vI2 = matI.applyTo(vI);
  vI3 = (qI1.toMatrix4x4()).applyTo(vI2);
  vI4 = (qI2.toMatrix4x4()).applyTo(vI3);
  vI5 = (qI3.toMatrix4x4()).applyTo(vI4);
  pushMatrix();
  translate(pivotPointLeftLeg.x+(2*vH5.x)+vI5.x, pivotPointLeftLeg.y-(2*vH5.z)-vI5.z, pivotPointLeftLeg.z-(2*vH5.y)-vI5.y);
  rotate(qI1.toAxisAngle()[0], -qI1.toAxisAngle()[1], qI1.toAxisAngle()[3], qI1.toAxisAngle()[2]);
  rotate(qI2.toAxisAngle()[0], -qI2.toAxisAngle()[1], qI2.toAxisAngle()[3], qI2.toAxisAngle()[2]);
  rotate(qI3.toAxisAngle()[0], -qI3.toAxisAngle()[1], qI3.toAxisAngle()[3], qI3.toAxisAngle()[2]);
  rotate(axisI[0], -axisI[1], axisI[3], axisI[2]);
  fill(255,0,0,230);
  box(widthLCalf, heightLCalf, depthLCalf);
  popMatrix();
  
  popMatrix();
  
  println();
  println(-vETorso5.x, -vETorso5.y, vETorso5.z);
  println(-vA3.x,-vA3.y,vA3.z);
  println(-vB3.x,-vB3.y,vB3.z);
  println(-vC3.x,-vC3.y,vC3.z);
  println(-vD3.x,-vD3.y,vD3.z);
}

boolean stableState(){
  if((millis()-startTime) > 15000)
    return true;
  return false;
}

void keyPressed(){
  if(key == '1'){
    segEdit = 1;
  }
  if(key == '2'){
    segEdit = 2;
  }
  if(key == '3'){
    segEdit = 3;
  }
  if(key == '4'){
    segEdit = 4;
  }
  if(key == '5'){
    segEdit = 5;
  }
  if(key == 'q' || key == 'Q'){
    if(segEdit == 1){
      t1E++;
    }
    if(segEdit == 2){
      t1F++;
    }
    if(segEdit == 3){
      t1G++;
    }
    if(segEdit == 4){
      t1H++;
    }
    if(segEdit == 5){
      t1I++;
    }
  }
  if(key == 'a' || key == 'A'){
    if(segEdit == 1){
      t1E--;
    }
    if(segEdit == 2){
      t1F--;
    }
    if(segEdit == 3){
      t1G--;
    }
    if(segEdit == 4){
      t1H--;
    }
    if(segEdit == 5){
      t1I--;
    }
  }
  if(key == 'w' || key == 'W'){
    if(segEdit == 1){
      t2E++;
    }
    if(segEdit == 2){
      t2F++;
    }
    if(segEdit == 3){
      t2G++;
    }
    if(segEdit == 4){
      t2H++;
    }
    if(segEdit == 5){
      t2I++;
    }
  }
  if(key == 's' || key == 'S'){
    if(segEdit == 1){
      t2E--;
    }
    if(segEdit == 2){
      t2F--;
    }
    if(segEdit == 3){
      t2G--;
    }
    if(segEdit == 4){
      t2H--;
    }
    if(segEdit == 5){
      t2I--;
    }
  }
  if(key == 'e' || key == 'E'){
    if(segEdit == 1){
      t3E++;
    }
    if(segEdit == 2){
      t3F++;
    }
    if(segEdit == 3){
      t3G++;
    }
    if(segEdit == 4){
      t3H++;
    }
    if(segEdit == 5){
      t3I++;
    }
  }
  if(key == 'd' || key == 'D'){
    if(segEdit == 1){
      t3E--;
    }
    if(segEdit == 2){
      t3F--;
    }
    if(segEdit == 3){
      t3G--;
    }
    if(segEdit == 4){
      t3H--;
    }
    if(segEdit == 5){
      t3I--;
    }
  }
  if(key == 'P' || key == 'p'){
    if(segEdit == 1){
      t1E+=10;
    }
    if(segEdit == 2){
      t1F+=10;
    }
    if(segEdit == 3){
      t1G+=10;
    }
    if(segEdit == 4){
      t1H+=10;
    }
    if(segEdit == 5){
      t1I+=10;
    }
  }
  if(key == ';' || key == ':'){
    if(segEdit == 1){
      t1E-=10;
    }
    if(segEdit == 2){
      t1F-=10;
    }
    if(segEdit == 3){
      t1G-=10;
    }
    if(segEdit == 4){
      t1H-=10;
    }
    if(segEdit == 5){
      t1I-=10;
    }
  }
  if(key == 'f' || key == 'F'){
    rotYDegrees = rotYDegrees-1;
  }
  if(key == 'r' || key == 'R'){
    rotYDegrees = rotYDegrees+1;
  }
  if(key == 'g' || key == 'G'){
    rotYDegrees = rotYDegrees-10;
  }
  if(key == 't' || key == 'T'){
    rotYDegrees = rotYDegrees+10;
  }
  if(key == 'c' || key == 'C'){
    calibBool = true;
  }
}

void serialEvent(Serial port) {
    interval = millis();
    if(port == portRightBicep){//checks to see which sensor the serialEvent is triggered for
      while (port.available() > 0) {//reads data only if the port is available
          int ch = port.read();//reads each byte of data as an int
          
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n')) {//used to check throughout the data packet if transmission is out of sync
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {//starts recording the data
              dataPacket[serialCount++] = (char)ch;//records the data into the dataPacket array
              if (serialCount == 14) {//runs this code when the packet of data is done
                  serialCount = 0; // restart packet byte position
                  
                  //Gets quaternion data from the dataPacket array
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  //Sets the toxicLibs quaternion to the new data from the transmission
                  quatA.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portRightForearm){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatB.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portLeftBicep){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatC.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portLeftForearm){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatD.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portTorso){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatE.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portRightThigh){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatF.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portRightCalf){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatG.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portRightCalf){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatG.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portLeftThigh){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatH.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portLeftCalf){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatI.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
}
