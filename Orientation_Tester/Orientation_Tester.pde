import toxi.geom.*;
import toxi.processing.*;

ToxiclibsSupport gfx;

Quaternion quatRightDefault = new Quaternion(1,0,0,0);
Quaternion quatLeftDefault = new Quaternion(0,0,0,1);
Quaternion quatUpDefault = new Quaternion(.71,0,-.71,0);

float[] axisA;
Matrix4x4 matA;
Vec3D vA = new Vec3D(200/2,0,0);
Vec3D vA2 = new Vec3D();

void setup(){
  size(600,600,P3D);
  
}

void draw(){
  background(0);
  
  pushMatrix();
  axisA = quatUpDefault.toAxisAngle();
  matA = quatUpDefault.toMatrix4x4();
  vA2 = matA.applyTo(vA);
  
  fill(255,0,0,230);
  translate(width/2+vA2.x, height/2-vA2.z, -vA2.y);
  rotate(axisA[0], -axisA[1], axisA[3], axisA[2]);
  box(200,100,50);
  popMatrix();
}
