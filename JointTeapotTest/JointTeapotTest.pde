import processing.serial.*;
import processing.opengl.*;
import toxi.geom.*;
import toxi.processing.*;

ToxiclibsSupport gfx;

float startTime = 0;
boolean recorded = false;
int serialIteration = 0;

Serial port;
char[] teapotPacket = new char[14];
int serialCount = 0;
int synced = 0;
int interval = 0;
int sensorNum = 1;

float[] q = new float[4];
float[] q2 = new float[4];

int widthA = 200;
int heightA = 20;
int depthA = 100;

int widthB = 180;
int heightB = 16;
int depthB = 20;

Quaternion quatA = new Quaternion(1,0,0,0);
float axisA[];
Matrix4x4 matA;
Vec3D vA = new Vec3D(widthA/2, 0, 0);
Vec3D vA2 = new Vec3D();
Quaternion quatAReturner = new Quaternion(1,0,0,0);
float AReturnerAxis[];
Matrix4x4 AReturnerMatrix;
Vec3D vA3 = new Vec3D();
Vec3D v = new Vec3D(0,heightA/2,0);
Vec3D vARight1 = new Vec3D(widthA, depthA/2,0);
Vec3D vARight2 = new Vec3D();
Vec3D vARight2a = new Vec3D();
Vec3D vARight3 = new Vec3D();
Vec3D vARight4 = new Vec3D();
Vec3D vARight5 = new Vec3D();
Vec3D vARight3a = new Vec3D();
Vec3D vARight4a = new Vec3D();
Vec3D vARight5a = new Vec3D();
Vec3D pivotPointRightArm = new Vec3D();

Quaternion quatB = new Quaternion(1,0,0,0);
float axisB[];
Matrix4x4 matB;
Vec3D vB = new Vec3D(widthB/2, 0, 0);
Vec3D vB2 = new Vec3D();
Quaternion quatBReturner = new Quaternion(1,0,0,0);
float BReturnerAxis[];
Matrix4x4 BReturnerMatrix;
Vec3D vB3 = new Vec3D();

Vec3D pivotPoint = new Vec3D();

boolean calibBool = false;
boolean calibBool2 = false;

Quaternion quatRightDefault = new Quaternion(1,0,0,0);
Quaternion quatLeftDefault = new Quaternion(0,0,0,1);
Quaternion quatUpDefault = new Quaternion(.71,.71,0,0);
Quaternion quatUpHelper = new Quaternion(.71,0,0,.71);
Quaternion quatOutDefault = new Quaternion(.71,0,-.71,0);

Quaternion quatTest = new Quaternion(1,0,0,0);
Quaternion quatTest2 = new Quaternion(1,0,0,0);
Quaternion quatTest3 = new Quaternion(1,0,0,0);
int t1 = 0;
int t2 = 0;
int t3 = 0;

void setup(){
  size(1200, 600, OPENGL);
  gfx = new ToxiclibsSupport(this);
  lights();
  smooth();
  //println(Serial.list());
  String portName = Serial.list()[0];
  port = new Serial(this, portName, 500000);
  
  pivotPoint = new Vec3D((width/2), (height/2), 0);
  
  port.write('r');
  port.write('r');
  port.write('r');
  port.write('r');
}

void draw(){
  if (millis() - interval > 1000){
    port.write('r');
    interval = millis();
    serialIteration = 0;
    recorded = false;
  }
  
  /*if(calibBool){
    quatAReturner = ((quatA.getConjugate()).multiply(quatUpDefault));
    quatBReturner = ((quatB.getConjugate()).multiply(quatUpDefault));
    quatTest = ((quatA.getConjugate()).multiply(quatOutDefault));
    calibBool = false;
    println("Steady state reached and inverse quats recorded!");
  }
  */
  lights();
  smooth();
  background(0);
  quatTest = Quaternion.createFromAxisAngle(new Vec3D(1,0,0),radians(t1));
  quatTest2 = Quaternion.createFromAxisAngle(new Vec3D(0,1,0),radians(t2));
  quatTest3 = Quaternion.createFromAxisAngle(new Vec3D(0,0,1),radians(t3));
  //quatTest = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(90));
  float[] testQuat = quatTest.toAxisAngle();
  float[] testQuat2 = quatTest2.toAxisAngle();
  float[] testQuat3 = quatTest3.toAxisAngle();
  AReturnerAxis = quatAReturner.toAxisAngle();
  AReturnerMatrix = quatAReturner.toMatrix4x4();
  axisA = quatA.toAxisAngle();
  matA = quatA.toMatrix4x4();
  vA2 = matA.applyTo(vA);
  vA3 = AReturnerMatrix.applyTo(vA2);
  vARight2 = matA.applyTo(vARight1);
  vARight3a = vARight2.copy();
  vARight4a = vARight2.copy();
  vARight5a = vARight2.copy();
  vARight3 = (quatTest.toMatrix4x4()).applyTo(vARight2);
  vARight4 = (quatTest2.toMatrix4x4()).applyTo(vARight2);
  vARight5 = (quatTest3.toMatrix4x4()).applyTo(vARight2);
  if(calibBool2){
    println("3a: " + (vARight3a));
    println("3: " + (vARight3));
    println("4: " + (vARight4));
    if((((-vARight3.y) >  0) && ((-vARight3.y) < (-vARight3a.y))) || (((-vARight3.y) <  0) && ((-vARight3.y) > (-vARight3a.y)))){
      vARight3a = vARight3.copy();
      println("good");
    }
    if((-vARight4.z) < (-vARight4a.z)){
      vARight4a = vARight4.copy();
      println("good2");
    }
    if((vARight5.x) > (vARight5a.x)){
      vARight5a = vARight5.copy();
      println("good3");
    }
  }
  pivotPointRightArm.setX(vARight3.x);
  pivotPointRightArm.setY(-vARight3.z);
  pivotPointRightArm.setZ(-vARight3.y);
  //println(pivotPointRightArm);
  pushMatrix();
  translate(pivotPoint.x, pivotPoint.y, pivotPoint.z);
  /*
  if(calibBool2){
    rotate(testQuat[0], -testQuat[1], testQuat[3], testQuat[2]);
  }
  */
  rotate(testQuat[0], -testQuat[1], testQuat[3], testQuat[2]);
  rotate(testQuat2[0], -testQuat2[1], testQuat2[3], testQuat2[2]);
  rotate(testQuat3[0], -testQuat3[1], testQuat3[3], testQuat3[2]);
  pushMatrix();
  //translate(width/2, height/2); //use for stationary movement about center of mass
  translate(vA3.x, -vA3.z, -vA3.y);
  rotate(AReturnerAxis[0], -AReturnerAxis[1], AReturnerAxis[3], AReturnerAxis[2]);
  rotate(axisA[0], -axisA[1], axisA[3], axisA[2]);
  
  fill(255,0,0,230);
  box(widthA, heightA, depthA);
  popMatrix();
  /*
  BReturnerAxis = quatBReturner.toAxisAngle();
  BReturnerMatrix = quatAReturner.toMatrix4x4();
  axisB = quatB.toAxisAngle();
  matB = quatB.toMatrix4x4();
  vB2 = matB.applyTo(vB);
  vB3 = BReturnerMatrix.applyTo(vB2);
  pushMatrix();
  //translate(width/2, height/2); //use for stationary movement about center of mass
  translate((2*vA3.x)+vB3.x, -(2*vA3.z)-vB3.z, (-2*vA3.y)-vB3.y);
  rotate(BReturnerAxis[0], -BReturnerAxis[1], BReturnerAxis[3], BReturnerAxis[2]);
  rotate(axisB[0], -axisB[1], axisB[3], axisB[2]);
  fill(255,0,0,230);
  box(widthB, heightB, depthB);
  popMatrix();
  */
  
  popMatrix();
}

boolean stableState(){
  if((millis()-startTime) > 15000)
    return true;
  return false;
}

void keyPressed(){
  if(key == 'c' || key == 'C'){
    calibBool = true;
    calibBool2 = true;
  }
  if(key == 'q' || key == 'Q'){
    t1++;
  }
  if(key == 'a' || key == 'A'){
    t1--;
  }
  if(key == 'w' || key == 'W'){
    t2++;
  }
  if(key == 's' || key == 'S'){
    t2--;
  }
  if(key == 'e' || key == 'E'){
    t3++;
  }
  if(key == 'd' || key == 'D'){
    t3--;
  }
}

void serialEvent(Serial port) {
    if(serialIteration == 0){
      startTime = millis();
      serialIteration++;
      println("Started");
    }
    interval = millis();
    while (port.available() > 0) {
        int ch = port.read();

        if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
        synced = 1;
        //print ((char)ch);

        if ((serialCount == 1 && ch != 2)
            || (serialCount == 12 && ch != '\r')
            || (serialCount == 13 && ch != '\n'))  {
            serialCount = 0;
            synced = 0;
            return;
        }

        if ((serialCount > 0 || ch == '$') && sensorNum == 1) {
            teapotPacket[serialCount++] = (char)ch;
            if (serialCount == 14) {
                serialCount = 0; // restart packet byte position
                sensorNum = 2;
                
                // get quaternion from data packet
                q[0] = ((teapotPacket[2] << 8) | teapotPacket[3]) / 16384.0f;
                q[1] = ((teapotPacket[4] << 8) | teapotPacket[5]) / 16384.0f;
                q[2] = ((teapotPacket[6] << 8) | teapotPacket[7]) / 16384.0f;
                q[3] = ((teapotPacket[8] << 8) | teapotPacket[9]) / 16384.0f;
                for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                
                // set our toxilibs quaternion to new data
                quatA.set(q[0], q[1], q[2], q[3]);
            }
        }
        //println(teapotPacket);
        if ((serialCount > 0 || ch == '$') && sensorNum == 2) {
            teapotPacket[serialCount++] = (char)ch;
            if (serialCount == 14) {
                serialCount = 0; // restart packet byte position
                sensorNum = 1;
                
                // get quaternion from data packet
                q2[0] = ((teapotPacket[2] << 8) | teapotPacket[3]) / 16384.0f;
                q2[1] = ((teapotPacket[4] << 8) | teapotPacket[5]) / 16384.0f;
                q2[2] = ((teapotPacket[6] << 8) | teapotPacket[7]) / 16384.0f;
                q2[3] = ((teapotPacket[8] << 8) | teapotPacket[9]) / 16384.0f;
                for (int i = 0; i < 4; i++) if (q2[i] >= 2) q2[i] = -4 + q2[i];
                
                // set our toxilibs quaternion to new data
                quatB.set(q2[0], q2[1], q2[2], q2[3]);
            }
        }
    }
}
