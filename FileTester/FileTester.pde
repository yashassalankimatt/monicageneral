PrintWriter out;
int x;

void setup(){
  out = createWriter("testfile.txt"); 
  x = 0;
}

void draw(){
  out.println(x + "Test");
}

void keyPressed(){
  if(key == 'C' || key == 'c'){
    out.flush();
    out.close();
    exit();
  }
}
