import processing.serial.*;// importing the Serial library to establish communication with the Arduinos
import toxi.geom.*;// importing the toxiclibs geometry library
import toxi.processing.*;// importing the toxiclibs processing library

ToxiclibsSupport gfx;// creating an instance of the toxiclibs support object

// this section is for the variables used in the recording of the data from the Serial port
char[] dataPacket = new char[14];// contains the pure byte data from the serial port
int serialCount = 0;// keeps track of how many bytes have been recorded
int synced = 0;// keeps track if the transmission is in sync with the recording on the processing sketch
int interval = 0;// used to write to serial port again if transmission is detected as having stopeed
float[] q = new float[4];// quaternion float packet from byte data

int baudRate = 500000;

Body body;
Vec3D viewTranslator;

boolean calibBool = false;

// void estup()

void setup(){// void setup()
  size(1200,1000,P3D);
  gfx = new ToxiclibsSupport(this);
  lights();
  smooth();
  println((Object[])Serial.list());
  
  body = new Body();
  
  body.torso.port = new Serial(this, Serial.list()[6], baudRate);
  body.rightBicep.port = new Serial(this, Serial.list()[2], baudRate);
  body.leftBicep.port = new Serial(this, Serial.list()[4], baudRate);
  body.rightForearm.port = new Serial(this, Serial.list()[3], baudRate);
  body.leftForearm.port = new Serial(this, Serial.list()[5], baudRate);
  
  body.torso.pivotPoint = new Vec3D(0,0,0);
  viewTranslator = new Vec3D(body.torso.pivotPoint.x, 0, 0);
  
  body.torso.port.write('r');
  body.torso.port.write('r');
  body.torso.port.write('r');
  body.torso.port.write('r');
  body.leftBicep.port.write('r');
  body.leftBicep.port.write('r');
  body.leftBicep.port.write('r');
  body.leftBicep.port.write('r');
  body.rightBicep.port.write('r');
  body.rightBicep.port.write('r');
  body.rightBicep.port.write('r');
  body.rightBicep.port.write('r');
  body.leftForearm.port.write('r');
  body.leftForearm.port.write('r');
  body.leftForearm.port.write('r');
  body.leftForearm.port.write('r');
  body.rightForearm.port.write('r');
  body.rightForearm.port.write('r');
  body.rightForearm.port.write('r');
  body.rightForearm.port.write('r');
  
}// void setup()



// void draw()

void draw(){// void draw()
  body.torso.port.write('r');
  body.leftBicep.port.write('r');
  body.rightBicep.port.write('r');
  body.leftForearm.port.write('r');
  body.rightForearm.port.write('r');
  if (millis() - interval > 1000){
    body.torso.port.write('r');
    body.leftBicep.port.write('r');
    body.rightBicep.port.write('r');
    body.leftForearm.port.write('r');
    body.rightForearm.port.write('r');
    interval = millis();
  }

  // setup of the environment
  lights();
  smooth();
  background(0);
  
  // calibration things
  if(calibBool){
    body.calibrateAll();
    calibBool = false;
    println("Calibration has been run");
  }
  
  // drawing of the actual body
  pushMatrix();// push 0
  // some code has been skipped here, maybe this might be what is making it look weird
  translate((width/2), (height*3)/4, 0);
  
  body.display();
  
  popMatrix();// pop 0
}// void draw()




// void serialEvent()

void serialEvent(Serial port){// void serialEvent(Serial port)
  interval = millis();
  if (port == body.rightBicep.port) {
    while (port.available() > 0) {
      int ch = port.read();
      
      if (synced == 0 && ch != '$') return;
      synced = 1;
      
      if ((serialCount == 1 && ch != 2)
          || (serialCount == 12 && ch != '\r')
          || (serialCount == 13 && ch != '\n')) {
          serialCount = 0;
          synced = 0;
          return;
      }
      
      if ((serialCount > 0 || ch == '$')) {
        dataPacket[serialCount++] = (char)ch;
        if (serialCount == 14) {
          serialCount = 0;
          
          //Gets quaternion data from the dataPacket array
          q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
          q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
          q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
          q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
          for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
          
          body.rightBicep.quat.set(q[0], q[1], q[2], q[3]);
        }
      }
    }
  }
  if (port == body.rightForearm.port) {
    while (port.available() > 0) {
      int ch = port.read();
      
      if (synced == 0 && ch != '$') return;
      synced = 1;
      
      if ((serialCount == 1 && ch != 2)
          || (serialCount == 12 && ch != '\r')
          || (serialCount == 13 && ch != '\n')) {
          serialCount = 0;
          synced = 0;
          return;
      }
      
      if ((serialCount > 0 || ch == '$')) {
        dataPacket[serialCount++] = (char)ch;
        if (serialCount == 14) {
          serialCount = 0;
          
          //Gets quaternion data from the dataPacket array
          q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
          q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
          q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
          q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
          for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
          
          body.rightForearm.quat.set(q[0], q[1], q[2], q[3]);
        }
      }
    }
  }
  if (port == body.leftBicep.port) {
    while (port.available() > 0) {
      int ch = port.read();
      
      if (synced == 0 && ch != '$') return;
      synced = 1;
      
      if ((serialCount == 1 && ch != 2)
          || (serialCount == 12 && ch != '\r')
          || (serialCount == 13 && ch != '\n')) {
          serialCount = 0;
          synced = 0;
          return;
      }
      
      if ((serialCount > 0 || ch == '$')) {
        dataPacket[serialCount++] = (char)ch;
        if (serialCount == 14) {
          serialCount = 0;
          
          //Gets quaternion data from the dataPacket array
          q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
          q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
          q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
          q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
          for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
          
          body.leftBicep.quat.set(q[0], q[1], q[2], q[3]);
        }
      }
    }
  }
  if (port == body.leftForearm.port) {
    while (port.available() > 0) {
      int ch = port.read();
      
      if (synced == 0 && ch != '$') return;
      synced = 1;
      
      if ((serialCount == 1 && ch != 2)
          || (serialCount == 12 && ch != '\r')
          || (serialCount == 13 && ch != '\n')) {
          serialCount = 0;
          synced = 0;
          return;
      }
      
      if ((serialCount > 0 || ch == '$')) {
        dataPacket[serialCount++] = (char)ch;
        if (serialCount == 14) {
          serialCount = 0;
          
          //Gets quaternion data from the dataPacket array
          q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
          q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
          q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
          q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
          for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
          
          body.leftForearm.quat.set(q[0], q[1], q[2], q[3]);
        }
      }
    }
  }
  if (port == body.torso.port) {
    while (port.available() > 0) {
      int ch = port.read();
      
      if (synced == 0 && ch != '$') return;
      synced = 1;
      
      if ((serialCount == 1 && ch != 2)
          || (serialCount == 12 && ch != '\r')
          || (serialCount == 13 && ch != '\n')) {
          serialCount = 0;
          synced = 0;
          return;
      }
      
      if ((serialCount > 0 || ch == '$')) {
        dataPacket[serialCount++] = (char)ch;
        if (serialCount == 14) {
          serialCount = 0;
          
          //Gets quaternion data from the dataPacket array
          q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
          q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
          q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
          q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
          for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
          
          body.torso.quat.set(q[0], q[1], q[2], q[3]);
        }
      }
    }
  }
}//void serialEvent(Serial port)




// void keyPressed()

void keyPressed(){// void keyPressed()
  if(key == 'c' || key == 'C')
    calibBool = true;
}//void keyPressed()
