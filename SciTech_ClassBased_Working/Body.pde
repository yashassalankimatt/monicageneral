import processing.serial.*;
import toxi.geom.*;
import toxi.processing.*;

public class Body{
  LimbSegment torso;
  LimbSegment rightBicep;
  LimbSegment rightForearm;
  LimbSegment leftBicep;
  LimbSegment leftForearm;
  
  Vec3D rightPivotPointDef;
  Vec3D leftPivotPointDef;
  Vec3D rightPtQuat;// this is the default pivot point with the sensor data applied to it
  Vec3D leftPtQuat;
  Vec3D rightPtCal;// this is the pivot point with the calibration rotation applied to it as well
  Vec3D leftPtCal;
  
  
  public Body(){// public Body()
    torso = new LimbSegment("torso");
    torso.setDimensions(400,16,200);
    rightBicep = new LimbSegment("right arm: bicep");
    rightBicep.setDimensions(200,20,30);
    rightBicep.setColor(color(0,0,255,230));
    rightForearm = new LimbSegment("right arm: forearm");
    rightForearm.setDimensions(180,16,20);
    leftBicep = new LimbSegment("left arm: bicep");
    leftBicep.setDimensions(200,20,30);
    leftForearm = new LimbSegment("left arm: forearm");
    leftForearm.setDimensions(180,16,20);
    rightPivotPointDef = new Vec3D(torso.w, torso.d/2, 0);
    leftPivotPointDef = new Vec3D(torso.w, -torso.d/2, 0);
    calculateVals();
  }// public Body()
  
  
  
  public void calibrateAll(){
    int startTime = millis();
    println("Starting calibrtion. Hold a T-pose for 10 seconds");
    while(abs(millis()-startTime) < 1000){
      // Do nothing while waiting
      // maybe print a countdown?
    }
    println("Calibrating now");
    torso.calibrate();
    rightBicep.calibrate();
    rightForearm.calibrate();
    leftBicep.calibrate();
    leftForearm.calibrate();
  }
  
  public void calculateVals(){// public void calculateVals()
    torso.calculate();
    rightBicep.calculate();
    rightForearm.calculate();
    leftBicep.calculate();
    leftForearm.calculate();
    
    rightPtQuat = torso.quatMat.applyTo(rightPivotPointDef);
    rightPtCal = torso.quatReturnerMat.applyTo(rightPtQuat);
    leftPtQuat = torso.quatMat.applyTo(leftPivotPointDef);
    leftPtCal = torso.quatReturnerMat.applyTo(leftPtQuat);
    rightBicep.pivotPoint.setX(torso.pivotPoint.x + rightPtQuat.x);
    rightBicep.pivotPoint.setY(torso.pivotPoint.y - rightPtQuat.z);
    rightBicep.pivotPoint.setZ(torso.pivotPoint.z - rightPtQuat.y);
    leftBicep.pivotPoint.setX(torso.pivotPoint.x + leftPtQuat.x);
    leftBicep.pivotPoint.setY(torso.pivotPoint.y - leftPtQuat.z);
    leftBicep.pivotPoint.setZ(torso.pivotPoint.z - leftPtQuat.y);
    
    rightForearm.pivotPoint.setX(rightBicep.pivotPoint.x+(2*rightBicep.calibratedCompVector.x));
    rightForearm.pivotPoint.setY(rightBicep.pivotPoint.y-(2*rightBicep.calibratedCompVector.z));
    rightForearm.pivotPoint.setZ(rightBicep.pivotPoint.z-(2*rightBicep.calibratedCompVector.y));
    leftForearm.pivotPoint.setX(leftBicep.pivotPoint.x+(2*leftBicep.calibratedCompVector.x));
    leftForearm.pivotPoint.setY(leftBicep.pivotPoint.y-(2*leftBicep.calibratedCompVector.z));
    leftForearm.pivotPoint.setZ(leftBicep.pivotPoint.z-(2*leftBicep.calibratedCompVector.y));
    
    torso.calculateTranslateVector();
    rightBicep.calculateTranslateVector();
    rightForearm.calculateTranslateVector();
    leftBicep.calculateTranslateVector();
    leftForearm.calculateTranslateVector();
  }// public void calculateVals()
  
  public void display(){// public void display()
    // this method is to house all of the draw commands for the body, and this is to be the
    // only thing to be called in the draw function to draw the body
    // this is to be completed once it has been proven that the basic class structure works
    body.calculateVals();
  
    pushMatrix();// push 1
    translate(rightBicep.translateVector.x, rightBicep.translateVector.y, rightBicep.translateVector.z);
    rotate(body.rightBicep.quatReturnerAxis[0], -body.rightBicep.quatReturnerAxis[1], body.rightBicep.quatReturnerAxis[3], body.rightBicep.quatReturnerAxis[2]);
    rotate(body.rightBicep.quatAxis[0], -body.rightBicep.quatAxis[1], body.rightBicep.quatAxis[3], body.rightBicep.quatAxis[2]);
    fill(body.rightBicep.col);
    box(body.rightBicep.w, body.rightBicep.h, body.rightBicep.d);
    popMatrix();// pop 1
    
    
    pushMatrix();// push 2
    translate(body.rightForearm.translateVector.x, body.rightForearm.translateVector.y, body.rightForearm.translateVector.z);
    rotate(body.rightForearm.quatReturnerAxis[0], -body.rightForearm.quatReturnerAxis[1], body.rightForearm.quatReturnerAxis[3], body.rightForearm.quatReturnerAxis[2]);
    rotate(body.rightForearm.quatAxis[0], -body.rightForearm.quatAxis[1], body.rightForearm.quatAxis[3], body.rightForearm.quatAxis[2]);
    fill(body.rightForearm.col);
    box(body.rightForearm.w, body.rightForearm.h, body.rightForearm.d);
    popMatrix();// pop 2
    
    pushMatrix();// push 3
    translate(body.leftBicep.translateVector.x, body.leftBicep.translateVector.y, body.leftBicep.translateVector.z);
    rotate(body.leftBicep.quatReturnerAxis[0], -body.leftBicep.quatReturnerAxis[1], body.leftBicep.quatReturnerAxis[3], body.leftBicep.quatReturnerAxis[2]);
    rotate(body.leftBicep.quatAxis[0], -body.leftBicep.quatAxis[1], body.leftBicep.quatAxis[3], body.leftBicep.quatAxis[2]);
    fill(body.leftBicep.col);
    box(body.leftBicep.w, body.leftBicep.h, body.leftBicep.d);
    popMatrix();// pop 3
    
    pushMatrix();// push 4
    translate(body.leftForearm.translateVector.x, body.leftForearm.translateVector.y, body.leftForearm.translateVector.z);
    rotate(body.leftForearm.quatReturnerAxis[0], -body.leftForearm.quatReturnerAxis[1], body.leftForearm.quatReturnerAxis[3], body.leftForearm.quatReturnerAxis[2]);
    rotate(body.leftForearm.quatAxis[0], -body.leftForearm.quatAxis[1], body.leftForearm.quatAxis[3], body.leftForearm.quatAxis[2]);
    fill(body.leftForearm.col);
    box(body.leftForearm.w, body.leftForearm.h, body.leftForearm.d);
    popMatrix();// pop 4
    
    pushMatrix();// push 5
    translate(body.torso.translateVector.x, body.torso.translateVector.y, body.torso.translateVector.z);
    rotate(body.torso.quatReturnerAxis[0], -body.torso.quatReturnerAxis[1], body.torso.quatReturnerAxis[3], body.torso.quatReturnerAxis[2]);
    rotate(body.torso.quatAxis[0], -body.torso.quatAxis[1], body.torso.quatAxis[3], body.torso.quatAxis[2]);
    fill(body.torso.col);
    box(body.torso.w, body.torso.h, body.torso.d);
    popMatrix();// pop 5
  }// public void display()
  
}// public class Body
