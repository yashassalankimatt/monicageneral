import processing.serial.*;
import toxi.geom.*;
import toxi.processing.*;

public class LimbSegment{// public class LimbSegment
  Serial port;
  String name;
  
  boolean enabled = true;// if this is false, this limb does not receive data and is not drawn
  
  // these boolean values are to be used during calibration
  boolean torso = false;// maybe consider how a head would work?
  boolean arm = false;// only one shuld be true for any given object, arm, leg or torso
  boolean leg = false;
  boolean left = false;// only one should be true for any given arm or leg object, left or right, but not both
  boolean right = false;
  
  int w = 200;// width, default is set to be 200
  int h = 10;// height, default is set to be 10
  int d = 30;// depth, default is set to be 30
  color col = color(255,0,0,230);// color of the box, default is red with almost complete opacity
  
  // base variables required to draw and keep track of limb
  Vec3D pivotPoint = new Vec3D(0,0,0);
  Quaternion quat = new Quaternion(1,0,0,0);
  float quatAxis[];
  Matrix4x4 quatMat;
  Vec3D offsetVector = new Vec3D(w/2,0,0);
  Vec3D compensationVector;
  
  Vec3D translateVector = new Vec3D();
  
  // variables required for calibration
  Quaternion quatReturner = new Quaternion(1,0,0,0);
  float[] quatReturnerAxis;
  Matrix4x4 quatReturnerMat;
  Vec3D calibratedCompVector;
  
  public LimbSegment(String limbType){// public LimbSegment()
    name = limbType;
    limbType = limbType.toLowerCase();
    if (limbType.equals("torso"))
      torso = true;
    else if (limbType.contains("arm")){// else if (limbType.contains("arm"))
      arm = true;
      if (limbType.contains("left"))
        left = true;
      else if (limbType.contains("right"))
        right = true;
    }// else if (limbType.contains("arm"))
    else if (limbType.contains("leg")){// else if (limbType.contains("leg"))
      leg = true;
      if (limbType.contains("left"))
        left = true;
      else if (limbType.contains("right"))
        right = true;
    }// else if (limbType.contains("leg"))
  }// public LimbSegment()
  
  
  public void calculate(){// public void calculate()
    quatAxis = quat.toAxisAngle();
    quatMat = quat.toMatrix4x4();

    compensationVector = quatMat.applyTo(offsetVector);
   
  }// public void clculate()
  
  public void calculateTranslateVector(){// public void calculateTranslateVector()
    calculate();
    translateVector.setX(pivotPoint.x+calibratedCompVector.x);
    translateVector.setY(pivotPoint.y-calibratedCompVector.z);
    translateVector.setZ(pivotPoint.z-calibratedCompVector.y);
  }// public void calculateTranslateVector()
  
  public void calibrate(){// public void calibrate()
    if (right){
      quatReturner = quat.getConjugate();
    }
    else if (left){
      quatReturner = quat.getConjugate();
      
      
      
    }
    quatReturnerAxis = quatReturner.toAxisAngle();
    quatReturnerMat = quatReturner.toMatrix4x4();
    calibratedCompVector = quatReturnerMat.applyTo(compensationVector);
  }// public void calibrate()
  
  public void setDimensions(int wid, int hei, int dep) {// public void setDimensions(int wid, int hei, int dep)
    w = wid;
    h = hei;
    d = dep;
    offsetVector = new Vec3D(w/2,0,0);
  }// public void setDimensions(int wid, int hei, int dep)
  
  public void setColor(color c){// public void setColor(color c)
    this.col = c;
  }
  
  public void display(){
    if (enabled){
        pushMatrix();// push 1
        //put in right location
        translate(translateVector.x, translateVector.y, translateVector.z);
        //
        rotate(quatReturnerAxis[0], -quatReturnerAxis[1], quatReturnerAxis[3], quatReturnerAxis[2]);
        if(left){
          rotate(180,0,1,0);
        }
        
        rotate(quatAxis[0], -quatAxis[1], quatAxis[3], quatAxis[2]);
        //drawing
        fill(col);
        box(w, h, d);        
        popMatrix();// pop 1 
      }// if (enabled)
  }//display limb
  
  public void enable(){
    enabled = true;
  }
  
  public void disable(){
    enabled = false;
  }
  
}// public class LimbSegment
