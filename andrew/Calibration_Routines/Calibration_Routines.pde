import processing.serial.*;// importing the Serial library to establish communication with the Arduinos
import toxi.geom.*;// importing the toxiclibs geometry library
import toxi.processing.*;// importing the toxiclibs processing library

ToxiclibsSupport gfx;// creating an instance of the toxiclibs support object

// this section is for the variables used in the recording of the data from the Serial port
char[] dataPacket = new char[14];// contains the pure byte data from the serial port
int serialCount = 0;// keeps track of how many bytes have been recorded
int synced = 0;// keeps track if the transmission is in sync with the recording on the processing sketch
int interval = 0;// used to write to serial port again if transmission is detected as having stopeed
float[] q = new float[4];// quaternion float packet from byte data

int baudRate = 115200;// this is the baud rate of the Arduinos, make sure that this is the same as the actual baud rate

Body body;// declaring a Body object to initialize and use later

boolean calibBool = false;// true when it is time to run the calibration routine
int overallRotDegreesY = 0;// this is the variable to control overall rotation of the model in the y axis

// void setup()

void setup(){// void setup()
  size(1200,1000,P3D);// setting up the window and settings the renderer
  gfx = new ToxiclibsSupport(this);// this is to make it so that you can use the toxiclibs geometry with the base processing draw commands
  smooth(8);// sets the antialiasing level to 8x
  println((Object[])Serial.list());// prints all of the avialable serial ports
  
  body = new Body();// initialzes a Body object to use for the model
  
  // setting the port to get the data from for each limb segment
  //body.segList.get("torso").port = new Serial(this, Serial.list()[6], baudRate);
  //body.segList.get("rightBicep").port = new Serial(this, Serial.list()[2], baudRate);
  body.segList.get("leftBicep").port = new Serial(this, Serial.list()[2], baudRate);
  //body.segList.get("rightForearm").port = new Serial(this, Serial.list()[3], baudRate);
  //body.segList.get("leftForearm").port = new Serial(this, Serial.list()[5], baudRate);
  
  body.segList.get("torso").disable();
  body.segList.get("rightBicep").disable();
  //body.segList.get("leftBicep").disable();
  body.segList.get("rightForearm").disable();
  body.segList.get("leftForearm").disable();
  
  
  body.writePorts(4);// writing a character to all of the ports multiple times to start the data stream
  
}// void setup()



// void draw()

void draw(){// void draw()
  body.writePorts(1);// writing to the ports to keep the data stream going if something gets stuck
  if (millis() - interval > 1000){// if the port isn't getting data, write to the ports again. Doesn't work right and needs a redo
    body.writePorts(1);
    interval = millis();
  }// if (millis() - interval > 1000)

  // setup of the environment
  lights();// enabling lights in the environment
  background(0);// setting the background to be black
  
  // calibration things
  if(calibBool){
    body.calibrateAll();
    calibBool = false;
    println("Calibration has been run");
  }// if(calibBool)
  
  // drawing of the actual body
  pushMatrix();// push 0
  
  translate((width/2), (height*3)/4, 0);// translates the entire torso and body to the point specified
  rotateY(radians(overallRotDegreesY));// rotates the entire torso and body around the hip based on mouse movement
  
  body.display();
  
  popMatrix();// pop 0
  
  pushMatrix();
  translate((width/2), (height/2),0);
  fill(color(0,0,255));
  box(100,5,5);
  popMatrix();
  pushMatrix();
  translate((width/2), (height/2),0);
  fill(color(0,255,0));
  box(5,100,5);
  popMatrix();
}// void draw()




// void serialEvent()

void serialEvent(Serial port){// void serialEvent(Serial port)
  interval = millis();
  for (LimbSegment currLimb : body.segList.values()){// runs through all of the limbs and checks which one the serial event has been triggered for
    if (port == currLimb.port) {// checks if this is the right limb's port
      while (port.available() > 0) {// while the port is available, keep reading
        int ch = port.read();
        
        // initial synchronization
        if (synced == 0 && ch != '$') return;
        synced = 1;
        
        // checks if the packet is still in sync and discards the packet if it is not in sync
        if ((serialCount == 1 && ch != 2)
            || (serialCount == 12 && ch != '\r')
            || (serialCount == 13 && ch != '\n')) {
            serialCount = 0;
            synced = 0;
            return;
        }
        
        if ((serialCount > 0 || ch == '$')) {// if there is still data, keep reading it
          dataPacket[serialCount++] = (char)ch;
          if (serialCount == 14) {// if the data packet is empty
            serialCount = 0;
            
            //Gets quaternion data from the dataPacket array
            q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
            q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
            q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
            q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
            for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
            
            currLimb.quat.set(q[0], q[1], q[2], q[3]);
          }// if (serialCount == 14)
        }// if ((serialCount > 0 || ch == '$'))
      }// while (port.available() > 0)
    }// if (port == currLimb.port)
  }// for (LimbSegment currLimb : body.segList.values())
}//void serialEvent(Serial port)


// void keyPressed()

void keyPressed(){// void keyPressed()
  if(key == 'c' || key == 'C')// if c is pressed, run the calibration routine
    calibBool = true;
}//void keyPressed()

void mouseDragged(){// void mouseDragged()
  if (mouseX > pmouseX)
    ++overallRotDegreesY;// if the direction of the mouse movement is to the right, rotate the model to the right
   else if (mouseX < pmouseX)
     --overallRotDegreesY;// if the direction of the mouse movement is to the left, rotate the model to the right
}// void mouseDragged()
