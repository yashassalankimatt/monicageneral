import processing.serial.*;//importing the Serial library to establish communications with the Arduinos
import toxi.geom.*;//importing the toxicLibs geometry library
import toxi.processing.*;//importing the toxiclibs processing library

ToxiclibsSupport gfx;//creating an instance of the toxiclibs support object

float startTime = 0;//this denotes when the program has received a serialEvent and when it starts recording
boolean recorded = false;//denotes if it is time for the calibration inverse quats to be recorded
int serialIteration = 0;//denotes the number of iterations of the serialEvent port

//this section initializes the Serial ports for each of the Arduino-sensor systems
Serial portRightBicep;
Serial portRightForearm;
Serial portLeftBicep;
Serial portLeftForearm;
Serial portTorso;

//this section is for the variables used in the recording of the data from the Serial ports
char[] dataPacket = new char[14];//contains the pure byte data from the serial port
int serialCount = 0;//keeps track of how many bytes have been recorded
int synced = 0;//keeps track if the transmission is in sync with the recording on the processing sketch
int interval = 0;//used to write to serial port again if transmission is detected as having stopped
int sensorNum = 1;//not used now, but was used to keep track of sensors when more than one sensor was connected to one Arduino
float[] q = new float[4];//quaternion float packet from byte data
float[] q2 = new float[4];//second quaternion float packet from byte data, used previously when more than one sensor was connected to one Arduino

//dimensions of the boxes
int widthRBicep = 200;
int heightRBicep = 20;
int depthRBicep = 30;

int widthRForearm = 180;
int heightRForearm = 16;
int depthRForearm = 20;

int widthLBicep = 200;
int heightLBicep = 20;
int depthLBicep = 30;

int widthLForearm = 180;
int heightLForearm = 16;
int depthLForearm = 20;

int widthTorso = 400;
int heightTorso = 16;
int depthTorso = 200;

Quaternion quatA = new Quaternion(1,0,0,0);
float axisA[];
Matrix4x4 matA;
Vec3D vA = new Vec3D(widthRBicep/2, 0, 0);
Vec3D vA2 = new Vec3D();
Quaternion quatAReturner = new Quaternion(1,0,0,0);
float AReturnerAxis[];
Matrix4x4 AReturnerMatrix;
Vec3D vA3 = new Vec3D();

Quaternion quatB = new Quaternion(1,0,0,0);
float axisB[];
Matrix4x4 matB;
Vec3D vB = new Vec3D(widthRForearm/2, 0, 0);
Vec3D vB2 = new Vec3D();
Quaternion quatBReturner = new Quaternion(1,0,0,0);
float BReturnerAxis[];
Matrix4x4 BReturnerMatrix;
Vec3D vB3 = new Vec3D();

Quaternion quatC = new Quaternion(1,0,0,0);
float axisC[];
Matrix4x4 matC;
Vec3D vC = new Vec3D(widthLBicep/2, 0, 0);
Vec3D vC2 = new Vec3D();
Quaternion quatCReturner = new Quaternion(1,0,0,0);
float CReturnerAxis[];
Matrix4x4 CReturnerMatrix;
Vec3D vC3 = new Vec3D();

Quaternion quatLeftDefault = new Quaternion(0,0,0,1);

Quaternion quatD = new Quaternion(1,0,0,0);
float axisD[];
Matrix4x4 matD;
Vec3D vD = new Vec3D(widthLForearm/2, 0, 0);
Vec3D vD2 = new Vec3D();
Quaternion quatDReturner = new Quaternion(1,0,0,0);
float DReturnerAxis[];
Matrix4x4 DReturnerMatrix;
Vec3D vD3 = new Vec3D();

Quaternion quatE = new Quaternion(1,0,0,0);
float axisE[];
Matrix4x4 matE;
Vec3D vE = new Vec3D(widthTorso/2, 0, 0);
Vec3D vE2 = new Vec3D();
float ECalibDegrees = 0;
Quaternion ECalibQuat = new Quaternion();
float ECalibAxis[];
Matrix4x4 ECalibMat;
Vec3D vE3 = new Vec3D();
Vec3D vELeft1 = new Vec3D(widthTorso, -depthTorso/2, 0);
Vec3D vERight1 = new Vec3D(widthTorso, depthTorso/2, 0);
Vec3D vELeft2 = new Vec3D();
Vec3D vERight2 = new Vec3D();
Vec3D vELeft3 = new Vec3D();
Vec3D vERight3 = new Vec3D();

float rotYDegrees = 0;
Quaternion rotYQuat = new Quaternion();
float[] rotYAxis;
Matrix4x4 rotYMat;
Vec3D viewTranslator1 = new Vec3D();
Vec3D viewTranslator2 = new Vec3D();

Vec3D pivotPointTorso = new Vec3D();
Vec3D pivotPointRightArm = new Vec3D();
Vec3D pivotPointLeftArm = new Vec3D();

boolean calibBool = false;

void setup(){
  size(1200, 1000, P3D);
  gfx = new ToxiclibsSupport(this);
  lights();
  smooth();
  println(Serial.list());
  portRightBicep = new Serial(this, Serial.list()[3], 115200);
  portRightForearm = new Serial(this, Serial.list()[4], 115200);
  portLeftBicep = new Serial(this, Serial.list()[2], 115200);
  portLeftForearm = new Serial(this, Serial.list()[1], 115200);
  portTorso = new Serial(this,Serial.list()[0], 115200);
  
  
  pivotPointTorso = new Vec3D((width/2), (height*3)/4, 0);
  viewTranslator1 = new Vec3D(pivotPointTorso.x, 0, 0);
  
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portTorso.write('r');
  portTorso.write('r');
  portTorso.write('r');
  portTorso.write('r');
}

void draw(){
  portRightBicep.write('r');
  portRightForearm.write('r');
  portLeftBicep.write('r');
  portLeftForearm.write('r');
  portTorso.write('r');
  if (millis() - interval > 1000){
    portRightBicep.write('r');
    portRightForearm.write('r');
    portLeftBicep.write('r');
    portLeftForearm.write('r');
    portTorso.write('r');
    interval = millis();
    serialIteration = 0;
    recorded = false;
  }
  
  lights();
  smooth();
  background(0);
  
  if(calibBool){//if stableState has been reached and inverse quaternions have not been recorded
    quatAReturner = quatA.getConjugate();//gets the motion to return offset quaternion back to default position
    quatBReturner = quatB.getConjugate();
    quatCReturner = (quatC.getConjugate()).multiply(quatLeftDefault);//gets motion to return quaternion to quatLeftDefault
    quatDReturner = (quatD.getConjugate()).multiply(quatLeftDefault);
    //recorded = true;
    calibBool = false;
    println("Steady state reached and inverse quats recorded!");
  }
  pushMatrix();
  rotYQuat = Quaternion.createFromAxisAngle(new Vec3D(0,0,1),radians(rotYDegrees));
  rotYAxis = rotYQuat.toAxisAngle();
  rotYMat = rotYQuat.toMatrix4x4();
  viewTranslator2 = rotYMat.applyTo(viewTranslator1);
  translate(600-viewTranslator2.x,0,viewTranslator2.y);
  rotate(rotYAxis[0], -rotYAxis[1], rotYAxis[3], rotYAxis[2]);
  //println(viewTranslator2);
  
  
  AReturnerAxis = quatAReturner.toAxisAngle();//converting to axisAngles to use the rotate function
  AReturnerMatrix = quatAReturner.toMatrix4x4();//converting to matrix in order to operate on vectors
  axisA = quatA.toAxisAngle();//converting to axisAngles to use the rotate function
  matA = quatA.toMatrix4x4();//converting to matrix in order to opertate on vectors
  vA2 = matA.applyTo(vA);//applies the actual sensor data rotation to a starting position vector
  vA3 = AReturnerMatrix.applyTo(vA2);//applies the calibration rotation to the sensor rotation
  pushMatrix();//pushes a matrix onto coordinate plane to individually manipulate this shape
  translate(pivotPointRightArm.x+vA3.x, pivotPointRightArm.y-vA3.z, pivotPointRightArm.z-vA3.y);//translates coordinate plane
  rotate(AReturnerAxis[0], -AReturnerAxis[1], AReturnerAxis[3], AReturnerAxis[2]);//rotates on calibration data
  rotate(axisA[0], -axisA[1], axisA[3], axisA[2]);//rotation based on sensor data
  fill(255,0,0,230);//defining the color and transparency
  box(widthRBicep, heightRBicep, depthRBicep);//creating a box with predefined dimensions
  popMatrix();//popping out of matrix to manipulate other object
  
  BReturnerAxis = quatBReturner.toAxisAngle();
  BReturnerMatrix = quatBReturner.toMatrix4x4();
  axisB = quatB.toAxisAngle();
  matB = quatB.toMatrix4x4();
  vB2 = matB.applyTo(vB);
  vB3 = BReturnerMatrix.applyTo(vB2);
  pushMatrix();
  translate(pivotPointRightArm.x+(2*vA3.x)+vB3.x, pivotPointRightArm.y-(2*vA3.z)-vB3.z, pivotPointRightArm.z+(-2*vA3.y)-vB3.y);
  rotate(BReturnerAxis[0], -BReturnerAxis[1], BReturnerAxis[3], BReturnerAxis[2]);
  rotate(axisB[0], -axisB[1], axisB[3], axisB[2]);
  fill(255,0,0,230);
  box(widthRForearm, heightRForearm, depthRForearm);
  popMatrix();
  
  CReturnerAxis = quatCReturner.toAxisAngle();
  CReturnerMatrix = quatCReturner.toMatrix4x4();
  axisC = quatC.toAxisAngle();
  matC = quatC.toMatrix4x4();
  vC2 = matC.applyTo(vC);
  vC3 = CReturnerMatrix.applyTo(vC2);
  pushMatrix();
  translate(pivotPointLeftArm.x+vC3.x, pivotPointLeftArm.y-vC3.z, pivotPointLeftArm.z-vC3.y);
  rotate(CReturnerAxis[0], -CReturnerAxis[1], CReturnerAxis[3], CReturnerAxis[2]);
  rotate(axisC[0], -axisC[1], axisC[3], axisC[2]);
  fill(255,0,0,230);
  box(widthLBicep, heightLBicep, depthLBicep);
  popMatrix();
  
  DReturnerAxis = quatDReturner.toAxisAngle();
  DReturnerMatrix = quatDReturner.toMatrix4x4();
  axisD = quatD.toAxisAngle();
  matD = quatD.toMatrix4x4();
  vD2 = matD.applyTo(vD);
  vD3 = DReturnerMatrix.applyTo(vD2);
  pushMatrix();
  translate(pivotPointLeftArm.x+(2*vC3.x)+vD3.x, pivotPointLeftArm.y-(2*vC3.z)-vD3.z, pivotPointLeftArm.z+(-2*vC3.y)-vD3.y);
  rotate(DReturnerAxis[0], -DReturnerAxis[1], DReturnerAxis[3], DReturnerAxis[2]);
  rotate(axisD[0], -axisD[1], axisD[3], axisD[2]);
  fill(255,0,0,230);
  box(widthLForearm, heightLForearm, depthLForearm);
  popMatrix();
  
  ECalibQuat = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(ECalibDegrees));
  ECalibAxis = ECalibQuat.toAxisAngle();
  ECalibMat = ECalibQuat.toMatrix4x4();
  axisE = quatE.toAxisAngle();
  matE = quatE.toMatrix4x4();
  vE2 = matE.applyTo(vE);
  vE3 = ECalibMat.applyTo(vE2);
  vELeft2 = matE.applyTo(vELeft1);
  vELeft3 = ECalibMat.applyTo(vELeft2);
  vERight2 = matE.applyTo(vERight1);
  vERight3 = ECalibMat.applyTo(vERight2);
  pivotPointRightArm.setX(pivotPointTorso.x+vERight3.x);
  pivotPointRightArm.setY(pivotPointTorso.y-vERight3.z);
  pivotPointRightArm.setZ(pivotPointTorso.z-vERight3.y);
  pivotPointLeftArm.setX(pivotPointTorso.x+vELeft3.x);
  pivotPointLeftArm.setY(pivotPointTorso.y-vELeft3.z);
  pivotPointLeftArm.setZ(pivotPointTorso.z-vELeft3.y);
  pushMatrix();
  translate(pivotPointTorso.x+vE3.x, pivotPointTorso.y-vE3.z, pivotPointTorso.z-vE3.y);
  rotate(ECalibAxis[0], -ECalibAxis[1], ECalibAxis[3], ECalibAxis[2]);
  rotate(axisE[0], -axisE[1], axisE[3], axisE[2]);
  fill(255,0,0,230);
  box(widthTorso, heightTorso, depthTorso);
  popMatrix();
  
  popMatrix();
}

boolean stableState(){
  if((millis()-startTime) > 15000)
    return true;
  return false;
}

void keyPressed(){
  if(key == 's' || key == 'S'){
    ECalibDegrees = ECalibDegrees-1;
  }
  if(key == 'w' || key == 'W'){
    ECalibDegrees = ECalibDegrees+1;
  }
  if(key == 'd' || key == 'D'){
    ECalibDegrees = ECalibDegrees-10;
  }
  if(key == 'e' || key == 'E'){
    ECalibDegrees = ECalibDegrees+10;
  }
  if(key == 'f' || key == 'F'){
    rotYDegrees = rotYDegrees-1;
  }
  if(key == 'r' || key == 'R'){
    rotYDegrees = rotYDegrees+1;
  }
  if(key == 'g' || key == 'G'){
    rotYDegrees = rotYDegrees-10;
  }
  if(key == 't' || key == 'T'){
    rotYDegrees = rotYDegrees+10;
  }
  if(key == 'c' || key == 'C'){
    calibBool = true;
  }
}

void serialEvent(Serial port) {
    interval = millis();
    if(port == portRightBicep){//checks to see which sensor the serialEvent is triggered for
      while (port.available() > 0) {//reads data only if the port is available
          int ch = port.read();//reads each byte of data as an int
          
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n')) {//used to check throughout the data packet if transmission is out of sync
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {//starts recording the data
              dataPacket[serialCount++] = (char)ch;//records the data into the dataPacket array
              if (serialCount == 14) {//runs this code when the packet of data is done
                  serialCount = 0; // restart packet byte position
                  
                  //Gets quaternion data from the dataPacket array
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  //Sets the toxicLibs quaternion to the new data from the transmission
                  quatA.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portRightForearm){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatB.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portLeftBicep){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatC.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portLeftForearm){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatD.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
    if(port == portTorso){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatE.set(q[0], q[1], q[2], q[3]);
              }
          }
      }
    }
}
