import processing.serial.*;//importing the Serial library to establish communications with the Arduinos
import toxi.geom.*;//importing the toxicLibs geometry library
import toxi.processing.*;//importing the toxiclibs processing library

ToxiclibsSupport gfx;//creating an instance of the toxiclibs support object

float startTime = 0;//this denotes when the program has received a serialEvent and when it starts recording
boolean recorded = false;//denotes if it is time for the calibration inverse quats to be recorded
int serialIteration = 0;//denotes the number of iterations of the serialEvent port

//this section initializes the Serial ports for each of the Arduino-sensor systems
Serial portRightBicep;
Serial portRightForearm;
Serial portLeftBicep;
Serial portLeftForearm;
Serial portTorso;

//this section is for the variables used in the recording of the data from the Serial ports
char[] dataPacket = new char[14];//contains the pure byte data from the serial port
int serialCount = 0;//keeps track of how many bytes have been recorded
int synced = 0;//keeps track if the transmission is in sync with the recording on the processing sketch
int interval = 0;//used to write to serial port again if transmission is detected as having stopped
int sensorNum = 1;//not used now, but was used to keep track of sensors when more than one sensor was connected to one Arduino
float[] q = new float[4];//quaternion float packet from byte data
Quaternion quatCurrent = new Quaternion(1,0,0,0);

boolean calibBool = false;

Body test;

void setup(){
  size(1200, 1000, P3D);
  gfx = new ToxiclibsSupport(this);
  test = new Body();
  lights();
  smooth();
  println(Serial.list());
  portRightBicep = new Serial(this, Serial.list()[2], 500000);
  portRightForearm = new Serial(this, Serial.list()[3], 500000);
  portLeftBicep = new Serial(this, Serial.list()[4], 500000);
  portLeftForearm = new Serial(this, Serial.list()[5], 500000);
  portTorso = new Serial(this,Serial.list()[6], 500000);
  
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightBicep.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portRightForearm.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftBicep.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portLeftForearm.write('r');
  portTorso.write('r');
  portTorso.write('r');
  portTorso.write('r');
  portTorso.write('r');
}

void draw(){
  portRightBicep.write('r');
  portRightForearm.write('r');
  portLeftBicep.write('r');
  portLeftForearm.write('r');
  portTorso.write('r');
  if (millis() - interval > 1000){
    portRightBicep.write('r');
    portRightForearm.write('r');
    portLeftBicep.write('r');
    portLeftForearm.write('r');
    portTorso.write('r');
    interval = millis();
    serialIteration = 0;
    recorded = false;
  }
  
  test.display();
  
}

boolean stableState(){
  if((millis()-startTime) > 15000)
    return true;
  return false;
}

void keyPressed(){
  if(key == 's' || key == 'S'){
    test.torso.calibRotation(-1);
  }
  if(key == 'w' || key == 'W'){
    test.torso.calibRotation(1);
  }
  if(key == 'd' || key == 'D'){
    test.torso.calibRotation(-10);
  }
  if(key == 'e' || key == 'E'){
    test.torso.calibRotation(10);
  }
  if(key == 'f' || key == 'F'){
    test.rotYDegrees = test.rotYDegrees-1;
  }
  if(key == 'r' || key == 'R'){
    test.rotYDegrees = test.rotYDegrees+1;
  }
  if(key == 'g' || key == 'G'){
    test.rotYDegrees = test.rotYDegrees-10;
  }
  if(key == 't' || key == 'T'){
    test.rotYDegrees = test.rotYDegrees+10;
  }
  if(key == 'c' || key == 'C'){
    test.calibrateAll();
  }
}

void serialEvent(Serial port) {
    interval = millis();
    if(port == portRightBicep){//checks to see which sensor the serialEvent is triggered for
      while (port.available() > 0) {//reads data only if the port is available
          int ch = port.read();//reads each byte of data as an int
          
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n')) {//used to check throughout the data packet if transmission is out of sync
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {//starts recording the data
              dataPacket[serialCount++] = (char)ch;//records the data into the dataPacket array
              if (serialCount == 14) {//runs this code when the packet of data is done
                  serialCount = 0; // restart packet byte position
                  
                  //Gets quaternion data from the dataPacket array
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  //Sets the toxicLibs quaternion to the new data from the transmission
                  quatCurrent.set(q[0], q[1], q[2], q[3]);
                  test.RBicep.update(quatCurrent);
                  println("1");
              }
          }
      }
    }
    if(port == portRightForearm){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatCurrent.set(q[0], q[1], q[2], q[3]);
                  test.RForearm.update(quatCurrent);
                  println("2");
              }
          }
      }
    }
    if(port == portLeftBicep){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatCurrent.set(q[0], q[1], q[2], q[3]);
                  test.LBicep.update(quatCurrent);
              }
          }
      }
    }
    if(port == portLeftForearm){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatCurrent.set(q[0], q[1], q[2], q[3]);
                  test.LForearm.update(quatCurrent);
              }
          }
      }
    }
    if(port == portTorso){
      while (port.available() > 0) {
          int ch = port.read();
  
          if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
          synced = 1;
          //print ((char)ch);
  
          if ((serialCount == 1 && ch != 2)
              || (serialCount == 12 && ch != '\r')
              || (serialCount == 13 && ch != '\n'))  {
              serialCount = 0;
              synced = 0;
              return;
          }
  
          if ((serialCount > 0 || ch == '$')) {
              dataPacket[serialCount++] = (char)ch;
              if (serialCount == 14) {
                  serialCount = 0; // restart packet byte position
                  
                  // get quaternion from data packet
                  q[0] = ((dataPacket[2] << 8) | dataPacket[3]) / 16384.0f;
                  q[1] = ((dataPacket[4] << 8) | dataPacket[5]) / 16384.0f;
                  q[2] = ((dataPacket[6] << 8) | dataPacket[7]) / 16384.0f;
                  q[3] = ((dataPacket[8] << 8) | dataPacket[9]) / 16384.0f;
                  for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                  
                  // set our toxilibs quaternion to new data
                  quatCurrent.set(q[0], q[1], q[2], q[3]);
                  test.torso.update(quatCurrent);
              }
          }
      }
    }
}
