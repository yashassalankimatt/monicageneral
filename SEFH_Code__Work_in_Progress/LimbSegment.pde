import processing.serial.*;//importing the Serial library to establish communications with the Arduinos
import toxi.geom.*;//importing the toxicLibs geometry library
import toxi.processing.*;//importing the toxiclibs processing library

public class LimbSegment {
  
  color limbColor;
  
  int widthSegment;
  int heightSegment;
  int depthSegment;
  int type;
  
  Quaternion sensorQuat = new Quaternion(1,0,0,0);
  float sensorAxis[];
  Matrix4x4 sensorMat = new Matrix4x4();
  Vec3D v1;
  Vec3D v2;
  Quaternion returnerQuat = new Quaternion(1,0,0,0);
  float[] returnerAxis;
  Matrix4x4 returnerMat = new Matrix4x4();
  Vec3D v3;
  
  float calibDegrees;
  Quaternion calibQuat = new Quaternion(1,0,0,0);
  float calibAxis[] = new float[4];
  Matrix4x4 calibMat = new Matrix4x4();
  
  Quaternion quatLeftDefault = new Quaternion(0,0,0,1);
  
  public LimbSegment(int w, int h, int d, int t, color c){//type 1 is right, type 2 is left, type 3 is vertical
    widthSegment = w;
    heightSegment = h;
    depthSegment = d;
    type = t;
    limbColor = c;
    sensorQuat = new Quaternion(1,0,0,0);
    v1 = new Vec3D(widthSegment/2, 0, 0);
    v2 = new Vec3D();
    returnerQuat = new Quaternion(1,0,0,0);
    v3 = new Vec3D();
    
    if(type == 2){
      calibDegrees = 0;
      calibQuat = new Quaternion();
    }
  }
  
  public void calculate(){
    returnerAxis = returnerQuat.toAxisAngle();
    returnerMat = returnerQuat.toMatrix4x4();
    sensorAxis = sensorQuat.toAxisAngle();
    sensorMat = sensorQuat.toMatrix4x4();
    v2 = sensorMat.applyTo(v1);
    if(type == 1 || type == 2){
      v3 = returnerMat.applyTo(v2);
    }
    if(type == 3){
      v3 = calibMat.applyTo(v2);
    }
  }
  
  public void update(Quaternion newQuat){
    sensorQuat = newQuat;
  }
  
  public void calibrate(){
    if(type == 1){
      returnerQuat = sensorQuat.getConjugate();
    }
    if(type == 2){
      returnerQuat = (sensorQuat.getConjugate()).multiply(quatLeftDefault);
    }
    if(type == 3){
      //insert interpolation calibration code here
    }
  }
  
  public void calibRotation(float n){
    calibDegrees = calibDegrees+n;
    calibQuat = Quaternion.createFromAxisAngle(new Vec3D(0,0,1), radians(calibDegrees));
    calibAxis = calibQuat.toAxisAngle();
    calibMat = calibQuat.toMatrix4x4();
  }
  
}
