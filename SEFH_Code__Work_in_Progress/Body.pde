import processing.serial.*;//importing the Serial library to establish communications with the Arduinos
import toxi.geom.*;//importing the toxicLibs geometry library
import toxi.processing.*;//importing the toxiclibs processing library

public class Body{
  
  float rotYDegrees = 0;
  Quaternion rotYQuat = new Quaternion();
  float[] rotYAxis;
  Matrix4x4 rotYMat;
  Vec3D viewTranslator1 = new Vec3D();
  Vec3D viewTranslator2 = new Vec3D();
  
  Vec3D pivotPointTorso = new Vec3D((width/2), (height*3)/4, 0);
  Vec3D vLeftArm1 = new Vec3D();
  Vec3D vRightArm1 = new Vec3D();
  Vec3D vLeftArm2 = new Vec3D();
  Vec3D vRightArm2 = new Vec3D();
  Vec3D vLeftArm3 = new Vec3D();
  Vec3D vRightArm3 = new Vec3D();
  Vec3D pivotPointRightArm = new Vec3D();
  Vec3D pivotPointLeftArm = new Vec3D();
  
  int widthTorso = 400;
  int heightTorso = 16;
  int depthTorso = 200;
  
  int widthRBicep = 200;
  int heightRBicep = 20;
  int depthRBicep = 30;
  
  int widthRForearm = 180;
  int heightRForearm = 16;
  int depthRForearm = 20;
  
  int widthLBicep = 200;
  int heightLBicep = 20;
  int depthLBicep = 30;
  
  int widthLForearm = 180;
  int heightLForearm = 16;
  int depthLForearm = 20;
  
  LimbSegment torso;
  LimbSegment RBicep;
  LimbSegment RForearm;
  LimbSegment LBicep;
  LimbSegment LForearm;
  
  public Body(){
    torso = new LimbSegment(widthTorso, heightTorso, depthTorso, 3, color(255,0,0,230));
    RBicep = new LimbSegment(widthRBicep, heightRBicep, depthRBicep, 1, color(255,0,0,230));
    RForearm = new LimbSegment(widthRForearm, heightRForearm, depthRForearm, 1, color(255,0,0,230));
    LBicep = new LimbSegment(widthLBicep, heightLBicep, depthLBicep, 2, color(255,0,0,230));
    LForearm = new LimbSegment(widthLForearm, heightLForearm, depthLForearm, 2, color(255,0,0,230));
    
    viewTranslator1 = new Vec3D(pivotPointTorso.x,0,0);
  }
  
  public void calculateAll(){
    calculateJoints();
    torso.calculate();
    RBicep.calculate();
    RForearm.calculate();
    LBicep.calculate();
    LForearm.calculate();
  }
  
  public void updateAll(Quaternion qTorso, Quaternion qRBicep, Quaternion qRForearm, Quaternion qLBicep, Quaternion qLForearm){
    torso.update(qTorso);
    RBicep.update(qRBicep);
    RForearm.update(qRForearm);
    LBicep.update(qLBicep);
    LForearm.update(qLForearm);
  }
  
  public void calibrateAll(){
    torso.calibrate();
    RBicep.calibrate();
    RForearm.calibrate();
    LBicep.calibrate();
    LForearm.calibrate();
    println("Calibrate all method run");
  }
  
  public void display(){
    lights();
    smooth();
    background(0);
    
    calculateAll();
    
    pushMatrix();
    /*
    rotYQuat = Quaternion.createFromAxisAngle(new Vec3D(0,0,1),radians(rotYDegrees));
    rotYAxis = rotYQuat.toAxisAngle();
    rotYMat = rotYQuat.toMatrix4x4();
    viewTranslator2 = rotYMat.applyTo(viewTranslator1);
    translate(600-viewTranslator2.x,0,viewTranslator2.y);
    rotate(rotYAxis[0], -rotYAxis[1], rotYAxis[3], rotYAxis[2]);
    //println(viewTranslator2);
    */
    translate(pivotPointTorso.x, pivotPointTorso.y, pivotPointTorso.z);
    
    pushMatrix();
    translate(torso.v3.x, -torso.v3.z, -torso.v3.y);
    rotate(torso.calibAxis[0], -torso.calibAxis[1], torso.calibAxis[3], torso.calibAxis[2]);
    rotate(torso.sensorAxis[0], -torso.sensorAxis[1], torso.calibAxis[3], torso.calibAxis[2]);
    fill(torso.limbColor);
    box(torso.widthSegment, torso.heightSegment, torso.depthSegment);
    popMatrix();
    
    pushMatrix();
    translate(pivotPointRightArm.x+RBicep.v3.x, pivotPointRightArm.y-RBicep.v3.z, pivotPointRightArm.z-RBicep.v3.y);
    rotate(RBicep.returnerAxis[0], -RBicep.returnerAxis[1], RBicep.returnerAxis[3], RBicep.returnerAxis[2]);
    rotate(RBicep.sensorAxis[0], -RBicep.sensorAxis[1], RBicep.sensorAxis[3], RBicep.sensorAxis[2]);
    fill(RBicep.limbColor);
    box(RBicep.widthSegment, RBicep.heightSegment, RBicep.depthSegment);
    popMatrix();
    
    
    
    popMatrix();
  }
  
  public void calculateJoints(){
    vLeftArm1 = new Vec3D(torso.widthSegment, -torso.depthSegment/2, 0);
    vRightArm1 = new Vec3D(torso.widthSegment, torso.depthSegment/2, 0);
    vLeftArm2 = torso.sensorMat.applyTo(vLeftArm1);
    vLeftArm3 = torso.calibMat.applyTo(vLeftArm2);
    vRightArm2 = torso.sensorMat.applyTo(vRightArm1);
    vRightArm3 = torso.calibMat.applyTo(vRightArm2);
    pivotPointRightArm.setX(pivotPointTorso.x+vRightArm3.x);
    pivotPointRightArm.setY(pivotPointTorso.y-vRightArm3.z);
    pivotPointRightArm.setZ(pivotPointTorso.z-vRightArm3.y);
    pivotPointLeftArm.setX(pivotPointTorso.x+vLeftArm3.x);
    pivotPointLeftArm.setY(pivotPointTorso.x-vLeftArm3.z);
    pivotPointLeftArm.setZ(pivotPointTorso.z-vRightArm3.y);
  }
  
}
