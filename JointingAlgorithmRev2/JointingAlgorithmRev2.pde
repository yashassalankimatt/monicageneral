/*
  The purpose of this program is to figure out a set of trigonometric calculations that can be done in order to make a 3D object rotate about its end in Processing.
  In the actual program, a rotate function with 4 parameters is used, but I don't understand how that works. Whenever I try to understand how it rotates objects based on its parameters, it changes the way that it rotates the object between each run.
  Right now, I have the first slider controlling the XY angle of box 1 and the third slider controlling the ZX angle of box 1. Box 2 and 4 do the same for the second box, but once I get the first box moving about its end, the second box should be easy.
*/

import toxi.geom.*;
import toxi.processing.*;

boolean resetAll = false;//contains the value if all sliders should be reset
String pi = "\u03C0";//this is the String code to print out the PI symbol

int width1 = 200;//width of the first box
int height1 = 50;//height of the first box
int depth1 = 90;//depth of the first box

int width2 = 100;//width of the second box
int height2 = 30;//height of the second box
int depth2 = 60;//depth of the second box

int width3 = 60;//width of the third box
int height3 = 20;//height of the third box
int depth3 = 40;//depth of the third box

int value1 = 300;//contains the raw pixel value of the first slider
int value2 = 300;//contains the raw pixel value of the second slider
int value3 = 300;//contains the raw pixel value of the third slider
int value4 = 300;//contains the raw pixel value of the fourth slider

float thetaA = 0;//contains the theta value about the axis of rotation for the Quaternion rotation for the first box
float axisArrayA [] = new float[3];//contains the unit Quaternion about which to be rotated
Quaternion quatA;//will contain the Quaternion, combining data from thetaA and axisArrayA
float[] axisA;//will contain the axis angles from quatA for rotation of a 3D object using the rotate function
Matrix4x4 matA;//will contain the 3D rotation from quatA in a 4D rotation matrix format
Vec3D vA = new Vec3D(width1/2,0,0);//contains the original vector that will be rotated using matA
Vec3D vA2 = new Vec3D();//will contain the manipulated vector of vA

float thetaB = 0;
float axisArrayB [] = new float[3];
Quaternion quatB;
float[] axisB;
Matrix4x4 matB;
Vec3D vB = new Vec3D(width2/2,0,0);
Vec3D vB2 = new Vec3D();

float thetaC = 0;
float axisArrayC [] = new float[3];
Quaternion quatC;
float[] axisC;
Matrix4x4 matC;
Vec3D vC = new Vec3D(width3/2,0,0);
Vec3D vC2 = new Vec3D();

void setup(){
  size(900,900, P3D);//sets up a window of width 900, height 900, and sets the renderer to be OpenGL
  //this section is initializing the axes of rotation for quaternions of each of the boxes
  axisArrayA[0] = 0.707107;
  axisArrayA[1] = 0.707107;
  axisArrayA[2] = 0;
  
  axisArrayB[0] = 0;
  axisArrayB[1] = 0.707107;
  axisArrayB[2] = 0.707107;
  
  axisArrayC[0] = 0.707107;
  axisArrayC[1] = 0;
  axisArrayC[2] = 0.707107;
}

void draw(){
  background(0);//sets the background to be black
  lights();//adds light to the rendering
  smooth();//adds antialiasing to the rendering
  printVariableMonitor();//starts the variable monitor at the bottom of the screen
  resetButtonHandler();//handles if the reset button has been pressed
  inputHandler();//handles input from the sliders
  constructBox1();//constructs the first box using inputs from the sliders
  constructBox2();//constructs the second box using inputs from the sliders
  constructBox3();//constructs the third box using inputs from the sliders
}

void constructBox1(){
  pushMatrix();//pushes the matrix for the first rectangle
  //translate(width / 2, height / 2);//use for just stationary
  quatA = new Quaternion(cos(thetaA), sin(thetaA)*(axisArrayA[0]), sin(thetaA)*(axisArrayA[1]), sin(thetaA)*(axisArrayA[2]));//makes a Quaternion from the angle and the axis of rotation
  axisA = quatA.toAxisAngle();//makes an axis array to use with rotation function- from quaternion
  //println("vA: " + vA);
  matA = quatA.toMatrix4x4();//makes a rotation matrix analogous to the rotation described by the quaternion
  vA2 = matA.applyTo(vA);//applies the rotation from the rotation matrix to the original vector and stores the resultant
  //println("vA2: " + vA2);
  translate((width/2)+vA2.x, (height/2)-vA2.z, -vA2.y);
  rotate(axisA[0], -axisA[1], axisA[3], axisA[2]);
  fill(255);//makes the fill of the box white
  box(width1,height1,depth1);//creates a box with set dimenstions here
  popMatrix();//pops out of rectangle 1 coordinate system
}

void constructBox2(){
  pushMatrix(); 
  quatB = new Quaternion(cos(thetaB), sin(thetaB)*(axisArrayB[0]), sin(thetaB)*(axisArrayB[1]), sin(thetaB)*(axisArrayB[2]));
  axisB = quatB.toAxisAngle();
  //println("vB: " + vB);
  matB = quatB.toMatrix4x4();
  vB2 = matB.applyTo(vB);
  //println("vB2: " + vB2);
  translate((width/2)+(2*vA2.x)+vB2.x, (height/2)+(-2*vA2.z)-vB2.z, (-2*vA2.y)-vB2.y);//use for just move center of mass at the end of box A
  rotate(axisB[0], -axisB[1], axisB[3], axisB[2]);
  fill(255);
  box(width2, height2, depth2);
  popMatrix();
}

void constructBox3(){
  pushMatrix(); 
  quatC = new Quaternion(cos(thetaC), sin(thetaC)*(axisArrayC[0]), sin(thetaC)*(axisArrayC[1]), sin(thetaC)*(axisArrayC[2]));
  axisC = quatC.toAxisAngle();
  //println("vC: " + vC);
  matC = quatC.toMatrix4x4();
  vC2 = matC.applyTo(vC);
  //println("vC2: " + vC2);
  translate((width/2)+(2*vA2.x)+(2*vB2.x)+vC2.x, (height/2)+(-2*vA2.z)+(-2*vB2.z)-vC2.z, (-2*vA2.y)+(-2*vB2.y)-vC2.y);//use for just move center of mass at the end of box A
  rotate(axisC[0], -axisC[1], axisC[3], axisC[2]);
  fill(255);
  box(width3, height3, depth3);
  popMatrix();
}

void resetButtonHandler(){
  fill(0, 0, 255);
  rect(775,825,75,50);
  fill(255);
  textSize(15);
  text("Reset", 795, 855);
  if(resetAll == true){
    value1 = 300;
    value2 = 300;
    value3 = 300;
    value4 = 300;
    resetAll = false;
  }
}

void inputHandler(){
  fill(0,0,255);//sets the fill color of the slider graphics to be blue
  rectMode(CENTER);
  rect(30, value1, 60, 20);//creates the rectangles for the sliders
  rect(100, value2, 60, 20);
  rect(170, value3, 60, 20);
  rect(240, value4, 60, 20);
  thetaA = map(value1, 0, 600, -2*PI, 2*PI);//calculates the theta value of the XY coordinate plane for the first box
  thetaB = map(value2, 0, 600, -2*PI, 2*PI);//calculates the theta value of the XY coordinate plane for the second box
  thetaC = map(value3, 0, 600, -2*PI, 2*PI);//calculates the theta value of the ZX coordinate plane for the first box
  //arr2 = map(value4, 0, 400, 1, 0);//calculates the theta value of the ZX coordinate plane for the second box
  rectMode(CORNER);
}

void printVariableMonitor(){
  textSize(15);
  fill(255);
  text("mouseX: " + mouseX, 800, 15);
  text("mouseY: " + mouseY, 800, 30);
  text("Variable Monitor", 10, 583);
  text("_______________", 10, 585);
  text("width1: " + width1, 10, 600);
  text("height1: " + height1, 10, 615);
  text("depth1: " + depth1, 10, 630);
  text("value1: " + value1, 10, 645);
  text("value2: " + value2, 10, 660);
  text("value3: " + value3, 10, 675);
  text("value4: " + value4, 10, 690);
  text("theta1: " + (thetaA/PI) + pi, 10, 705);
  text("arr0: " + axisArrayA[0], 10, 720);
  text("arr1: " + axisArrayA[1], 10, 735);
  text("arr2: " + axisArrayA[2], 10, 750);
  /*text(quatA.toString(), 10, 765);
  text("axis[0]: " + axisA[0], 10, 780);
  text("axis[1]: " + axisA[1], 10, 795);
  text("axis[2]: " + axisA[2], 10, 810);
  text("axis[3]: " + axisA[3], 10, 825);*/
  /*text("endPosY1: " + endPosY1, 10, 840);
  text("endPosZ1: " + endPosZ1, 10, 855);
  text("width2: " + width2, 225, 600);
  text("height2: " + height2, 225, 615);
  text("depth2: " + depth2, 225, 630);
  text("translateXXY2: " + translateXXY2, 225, 645);
  text("translateYXY2: " + translateYXY2, 225, 660);
  text("translateZZX2: " + translateZZX2, 225, 675);
  text("translateXZX2: " + translateXZX2, 225, 690);
  */
  //text("xA: " + xA, 225, 705);
  text("A: " + vA2.x, 225, 720);
  //text("z: " + zB, 225, 735);
  
}

void mouseDragged(){
  if(mouseX>0 && mouseX<65)//if the mouse is within the bounds of the first slider
    value1 = mouseY;
  if(mouseX>65 && mouseX<135)//if the mouse is within the bounds of the second slider
    value2 = mouseY;
  if(mouseX>135 && mouseX<205)
    value3 = mouseY;
  if(mouseX>205 && mouseX<275)
    value4 = mouseY;
}

void mouseClicked(){
  if(mouseX>775 && mouseX<850 && mouseY>825 && mouseY<875 && resetAll == false)
    resetAll = true;
}

void keyPressed(){
  if(resetAll == false && (key == 'r' || key == 'R'))
    resetAll = true;
  if(key == '1')
    value1 = 300;
  if(key == '2')
    value2 = 300;
  if(key == '3')
    value3 = 300;
  if(key == '4')
    value4 = 300;
}
