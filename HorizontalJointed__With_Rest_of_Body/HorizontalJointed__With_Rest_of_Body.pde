import processing.serial.*;
import toxi.geom.*;
import toxi.processing.*;

ToxiclibsSupport gfx;

float startTime = 0;
boolean recorded = false;
int serialIteration = 0;

Serial port;
char[] teapotPacket = new char[14];
int serialCount = 0;
int synced = 0;
int interval = 0;
int sensorNum = 1;

float[] q = new float[4];
float[] q2 = new float[4];

int widthA = 200;
int heightA = 20;
int depthA = 30;

int widthB = 180;
int heightB = 16;
int depthB = 20;

Quaternion quatA = new Quaternion(1,0,0,0);
float axisA[];
Matrix4x4 matA;
Vec3D vA = new Vec3D(widthA/2, 0, 0);
Vec3D vA2 = new Vec3D();
Quaternion quatAReturner = new Quaternion(1,0,0,0);
float AReturnerAxis[];
Matrix4x4 AReturnerMatrix;
Vec3D vA3 = new Vec3D();

Quaternion quatB = new Quaternion(1,0,0,0);
float axisB[];
Matrix4x4 matB;
Vec3D vB = new Vec3D(widthB/2, 0, 0);
Vec3D vB2 = new Vec3D();
Quaternion quatBReturner = new Quaternion(1,0,0,0);
float BReturnerAxis[];
Matrix4x4 BReturnerMatrix;
Vec3D vB3 = new Vec3D();

Vec3D pivotPoint = new Vec3D();

void setup(){
  size(1200, 800, P3D);
  gfx = new ToxiclibsSupport(this);
  lights();
  smooth();
  //println(Serial.list());
  String portName = Serial.list()[0];
  port = new Serial(this, portName, 115200);
  
  pivotPoint = new Vec3D((width/2)+25, (height/3), 0);
  
  port.write('r');
  port.write('r');
  port.write('r');
  port.write('r');
}

void draw(){
  if (millis() - interval > 1000){
    port.write('r');
    interval = millis();
    serialIteration = 0;
    recorded = false;
  }
  
  if(stableState() && !recorded){
    quatAReturner = quatA.getConjugate();
    quatBReturner = quatB.getConjugate();
    recorded = true;
    println("Steady state reached and inverse quats recorded!");
  }
  
  lights();
  smooth();
  background(0);
  
  AReturnerAxis = quatAReturner.toAxisAngle();
  AReturnerMatrix = quatAReturner.toMatrix4x4();
  axisA = quatA.toAxisAngle();
  matA = quatA.toMatrix4x4();
  vA2 = matA.applyTo(vA);
  vA3 = AReturnerMatrix.applyTo(vA2);
  pushMatrix();
  //translate(width/2, height/2); //use for stationary movement about center of mass
  translate(pivotPoint.x+vA3.x, pivotPoint.y-vA3.z, pivotPoint.z-vA3.y);
  rotate(AReturnerAxis[0], -AReturnerAxis[1], AReturnerAxis[3], AReturnerAxis[2]);
  rotate(axisA[0], -axisA[1], axisA[3], axisA[2]);
  fill(255,0,0,230);
  box(widthA, heightA, depthA);
  popMatrix();
  
  BReturnerAxis = quatBReturner.toAxisAngle();
  BReturnerMatrix = quatAReturner.toMatrix4x4();
  axisB = quatB.toAxisAngle();
  matB = quatB.toMatrix4x4();
  vB2 = matB.applyTo(vB);
  vB3 = BReturnerMatrix.applyTo(vB2);
  pushMatrix();
  //translate(width/2, height/2); //use for stationary movement about center of mass
  translate(pivotPoint.x+(2*vA3.x)+vB3.x, pivotPoint.y-(2*vA3.z)-vB3.z, pivotPoint.z+(-2*vA3.y)-vB3.y);
  rotate(BReturnerAxis[0], -BReturnerAxis[1], BReturnerAxis[3], BReturnerAxis[2]);
  rotate(axisB[0], -axisB[1], axisB[3], axisB[2]);
  fill(255,0,0,230);
  box(widthB, heightB, depthB);
  popMatrix();
  
  pushMatrix();
  translate(width/2, height/2);
  fill(255,0,0,230);
  box(50, 300, 50);
  popMatrix();
  
  pushMatrix();
  translate(width/2-90, height/2+225);
  rotateZ(PI/4);
  fill(255,0,0,230);
  box(50, 200, 50);
  popMatrix();
  
  pushMatrix();
  translate(width/2+90, height/2+225);
  rotateZ(-PI/4);
  fill(255,0,0,230);
  box(50, 200, 50);
  popMatrix();
}

boolean stableState(){
  if((millis()-startTime) > 1000)
    return true;
  return false;
}

void serialEvent(Serial port) {
    if(serialIteration == 0){
      startTime = millis();
      serialIteration++;
      println("Started");
    }
    interval = millis();
    while (port.available() > 0) {
        int ch = port.read();

        if (synced == 0 && ch != '$') return;   // initial synchronization - also used to resync/realign if needed
        synced = 1;
        //print ((char)ch);

        if ((serialCount == 1 && ch != 2)
            || (serialCount == 12 && ch != '\r')
            || (serialCount == 13 && ch != '\n'))  {
            serialCount = 0;
            synced = 0;
            return;
        }

        if ((serialCount > 0 || ch == '$') && sensorNum == 1) {
            teapotPacket[serialCount++] = (char)ch;
            if (serialCount == 14) {
                serialCount = 0; // restart packet byte position
                sensorNum = 2;
                
                // get quaternion from data packet
                q[0] = ((teapotPacket[2] << 8) | teapotPacket[3]) / 16384.0f;
                q[1] = ((teapotPacket[4] << 8) | teapotPacket[5]) / 16384.0f;
                q[2] = ((teapotPacket[6] << 8) | teapotPacket[7]) / 16384.0f;
                q[3] = ((teapotPacket[8] << 8) | teapotPacket[9]) / 16384.0f;
                for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];
                
                // set our toxilibs quaternion to new data
                quatA.set(q[0], q[1], q[2], q[3]);
            }
        }
        
        //println(teapotPacket);
        if ((serialCount > 0 || ch == '$') && sensorNum == 2) {
            teapotPacket[serialCount++] = (char)ch;
            if (serialCount == 14) {
                serialCount = 0; // restart packet byte position
                sensorNum = 1;
                
                // get quaternion from data packet
                q2[0] = ((teapotPacket[2] << 8) | teapotPacket[3]) / 16384.0f;
                q2[1] = ((teapotPacket[4] << 8) | teapotPacket[5]) / 16384.0f;
                q2[2] = ((teapotPacket[6] << 8) | teapotPacket[7]) / 16384.0f;
                q2[3] = ((teapotPacket[8] << 8) | teapotPacket[9]) / 16384.0f;
                for (int i = 0; i < 4; i++) if (q2[i] >= 2) q2[i] = -4 + q2[i];
                
                // set our toxilibs quaternion to new data
                quatB.set(q2[0], q2[1], q2[2], q2[3]);
            }
        }
    }
}
