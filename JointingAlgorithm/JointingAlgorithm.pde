/*
  The purpose of this program is to figure out a set of trigonometric calculations that can be done in order to make a 3D object rotate about its end in Processing.
  In the actual program, a rotate function with 4 parameters is used, but I don't understand how that works. Whenever I try to understand how it rotates objects based on its parameters, it changes the way that it rotates the object between each run.
  Right now, I have the first slider controlling the XY angle of box 1 and the third slider controlling the ZX angle of box 1. Box 2 and 4 do the same for the second box, but once I get the first box moving about its end, the second box should be easy.
*/

import toxi.geom.*;
import toxi.processing.*;

void setup(){
  size(900,900, OPENGL);//sets up a window of width 900, height 900, and sets the renderer to be OpenGL
}

boolean resetAll = false;//contains the value if all sliders should be reset

int width1 = 200;//width of the first box
int height1 = 50;//height of the first box
int depth1 = 90;//depth of the first box

int width2 = 100;//width of the second box
int height2 = 50;//height of the second box
int depth2 = 70;//depth of the second box

int value1 = 300;//contains the raw pixel value of the first slider
int value2 = 300;//contains the raw pixel value of the second slider
int value3 = 300;//contains the raw pixel value of the third slider
int value4 = 300;//contains the raw pixel value of the fourth slider
float thetaXY1 = 0;//contains the theta value of the XY coordinate plane for the first box
float thetaXY2 = 0;//contains the theta value of the XY coordinate plane for the second box
float thetaZX1 = 0;//contains the theta value of the ZX coordinate plane for the first box
float thetaZX2 = 0;//contains the theta value of the ZX coordinate plane for the second box
float translateXXY1 = 0;//contains the X value used to translate the first box to the center through XY movement
float translateYXY1 = 0;//contains the Y value used to translate the first box to the center through XY movement
float translateZZX1 = 0;//contains the Z value used to translate the first box to the center through ZX movement
float translateXZX1 = 0;//contains the X value used to translate the first box to the center through ZX movement
float endPosX1 = 0;//contains the x position of the end of the first box
float endPosY1 = 0;//contains the y position of the end of the first box
float endPosZ1 = 0;//contains the z position of the end of the first box
float translateXXY2 = 0;//contains the X value used to translate the second box to the center through XY movement
float translateYXY2 = 0;//contains the Y value used to translate the second box to the center through XY movement
float translateZZX2 = 0;//contains the Z value used to translate the second box to the center through ZX movement
float translateXZX2 = 0;//contains the X value used to translate the second box to the center through ZX movement
String pi = "\u03C0";
float x = 0;
float y = 0;
float z = 0;

void draw(){
  background(0);//sets the background to be black
  lights();
  smooth();
  //constructGrid();
  printVariableMonitor();
  resetButtonHandler();
  inputHandler();
  calculate();
  constructBox1();
  //constructBox2();
}

void calculate(){
  translateXXY1 = (width1/2)*(cos(thetaXY1));//x value used to translate the first, longer box to the center
  translateYXY1 = (width1/2)*(sin(thetaXY1));//y value used to translate the first, longer box to the center
  translateZZX1 = -depth1*(sin(thetaZX1));//z value used to translate the first, longer box to the center
  translateXZX1 = depth1*(cos(thetaZX1))-depth1;
  endPosX1 = (2*translateXXY1)+(2*translateXZX1);//x value for the end of the first box
  endPosY1 = (2*translateYXY1);//y value for the end of the first box
  endPosZ1 = (2*translateZZX1);//z value used for the end of the first box
  translateXXY2 = (width2/2)*(cos(thetaXY2));//x value used to translate the second rectanlge to the center of the first rectangle
  translateYXY2 = (width2/2)*(sin(thetaXY2));//y value used to translate the second rectanlge to the center of the first rectangle
  translateZZX2 = -depth2*(sin(thetaZX2));
  translateXZX2 = depth2*(cos(thetaZX2))-depth2;
}

void constructBox1(){
  pushMatrix();//pushes the matrix for the first rectangle
  translate((width/2)+translateXXY1+translateXZX1,(height/2)+translateYXY1, translateZZX1);//translates the coordinate system using the translate 1 values
  //translate(width / 2, height / 2);
  rotateZ(thetaXY1);//rotates the coordinate system the required angle after compensating through translation
  rotateY(thetaZX1);
  //rotate(10, thetaZX1, thetaXY1, 0);//something to do with QUATERNIONS. I HATE MY LIFE
  fill(255);//makes the fill of the box white
  box(width1,height1,depth1);//creates a box with set dimenstions here
  x = modelX(0,0,0)-(width/2);
  y = modelY(0,0,0)-(height/2);
  z = modelZ(0,0,0);
  popMatrix();//pops out of rectangle 1 coordinate system
}

void  constructGrid(){
  for(int x1 = 0; x < width; x+=10){
     fill(255,0,0);
     stroke(255,0,0);
     line(x1,0,x1,height);
  }
}

void constructBox2(){
  pushMatrix();//pushed matrix for the second rectangle
  translate((width/2)+endPosX1+translateXXY2+translateXZX2,(height/2)+endPosY1+translateYXY2, endPosZ1+translateZZX2);//translate the second box to the end of the first box through its rotations
  rotateZ(thetaXY2);//rotates the coordinate system from given value after compensation
  rotateY(thetaZX2);
  fill(255);
  box(100,50,70);//creates a box with set dimensions here
  popMatrix();//pops out of box 2 coordinate system
}

void resetButtonHandler(){
  fill(0, 0, 255);
  rect(775,825,75,50);
  fill(255);
  textSize(15);
  text("Reset", 795, 855);
  if(resetAll == true){
    value1 = 300;
    value2 = 300;
    value3 = 300;
    value4 = 300;
    resetAll = false;
  }
}

void inputHandler(){
  fill(0,0,255);//sets the fill color of the slider graphics to be blue
  rectMode(CENTER);
  rect(30, value1, 60, 20);//creates the rectangles for the sliders
  rect(100, value2, 60, 20);
  rect(170, value3, 60, 20);
  rect(240, value4, 60, 20);
  thetaXY1 = map(value1, 0, 600, -2*PI, 2*PI);//calculates the theta value of the XY coordinate plane for the first box
  thetaXY2 = map(value2, 0, 600, -2*PI, 2*PI);//calculates the theta value of the XY coordinate plane for the second box
  thetaZX1 = map(value3, 0, 600, -2*PI, 2*PI);//calculates the theta value of the ZX coordinate plane for the first box
  thetaZX2 = map(value4, 0, 600, -2*PI, 2*PI);//calculates the theta value of the ZX coordinate plane for the second box
  rectMode(CORNER);
}

void printVariableMonitor(){
  textSize(15);
  fill(255);
  text("mouseX: " + mouseX, 800, 15);
  text("mouseY: " + mouseY, 800, 30);
  text("Variable Monitor", 10, 583);
  text("_______________", 10, 585);
  text("width1: " + width1, 10, 600);
  text("height1: " + height1, 10, 615);
  text("depth1: " + depth1, 10, 630);
  text("value1: " + value1, 10, 645);
  text("value2: " + value2, 10, 660);
  text("value3: " + value3, 10, 675);
  text("value4: " + value4, 10, 690);
  text("thetaXY1: " + (thetaXY1/PI) + pi, 10, 705);
  text("thetaXY2: " + (thetaXY2/PI) + pi, 10, 720);
  text("thetaZX1: " + (thetaZX1/PI) + pi, 10, 735);
  text("thetaZX2: " + (thetaZX2/PI) + pi, 10, 750);
  text("translateXXY1: " + translateXXY1, 10, 765);
  text("translateYXY1: " + translateYXY1, 10, 780);
  text("translateZZX1: " + translateZZX1, 10, 795);
  text("translateXZX1: " + translateXZX1, 10, 810);
  text("endPosX1: " + endPosX1, 10, 825);
  text("endPosY1: " + endPosY1, 10, 840);
  text("endPosZ1: " + endPosZ1, 10, 855);
  text("width2: " + width2, 225, 600);
  text("height2: " + height2, 225, 615);
  text("depth2: " + depth2, 225, 630);
  text("translateXXY2: " + translateXXY2, 225, 645);
  text("translateYXY2: " + translateYXY2, 225, 660);
  text("translateZZX2: " + translateZZX2, 225, 675);
  text("translateXZX2: " + translateXZX2, 225, 690);
  text("x: " + x, 225, 705);
  text("y: " + y, 225, 720);
  text("z: " + z, 225, 735);
}

void mouseDragged(){
  if(mouseX>0 && mouseX<65)//if the mouse is within the bounds of the first slider
    value1 = mouseY;
  if(mouseX>65 && mouseX<135)//if the mouse is within the bounds of the second slider
    value2 = mouseY;
  if(mouseX>135 && mouseX<205)
    value3 = mouseY;
  if(mouseX>205 && mouseX<275)
    value4 = mouseY;
}

void mouseClicked(){
  if(mouseX>775 && mouseX<850 && mouseY>825 && mouseY<875 && resetAll == false)
    resetAll = true;
}

void keyPressed(){
  if(resetAll == false && (key == 'r' || key == 'R'))
    resetAll = true;
  if(key == '1')
    value1 = 300;
  if(key == '2')
    value2 = 300;
  if(key == '3')
    value3 = 300;
  if(key == '4')
    value4 = 300;
}
