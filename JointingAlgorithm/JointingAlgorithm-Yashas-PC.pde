
void setup(){
  size(600,600, OPENGL);
  lights();
  smooth();
}

int value1 = 300;
int value2 = 300;
int value3 = 300;
int value4 = 300;
float thetaXY1 = 0;
float thetaXY2 = 0;
float thetaZY1 = 0;
float thetaZX1 = 0;
float translateX1 = 0;
float translateY1 = 0;
float translateZ1 = 0;
float lengthX1 = 0;
float lengthY1 = 0;
float lengthZ1 = 0;
float translateX2 = 0;
float translateY2 = 0;
float translateZ2 = 0;

void draw(){
  background(0);//sets the background to be black
  fill(0,0,255);//sets the fill color of the slider graphics to be blue
  rect(0, value1, 60, 20);//creates the rectangles for the sliders
  rect(70, value2, 60, 20);
  rect(140, value3, 60, 20);
  rect(210, value4, 60, 20);
  thetaXY1 = map(value1, 0, 600, -2*PI, 2*PI);//calculates the theta values in radians based on the input from the sliders
  thetaXY2 = map(value2, 0, 600, -2*PI, 2*PI);
  thetaZY1 = map(value3, 0, 600, -2*PI, 2*PI);
  translateX1 = 100*(cos(thetaXY1));//x value used to translate the first, longer rectangle to the center
  translateY1 = 100*(sin(thetaXY1));//y value used to translate the first, longer rectangle to the center
  lengthX1 = 200*(cos(thetaXY1));//x value for the end of the first rectangle
  lengthY1 = 200*(sin(thetaXY1));//y value for the end of the first rectangle
  translateX2 = 50*(cos(thetaXY2));//x value used to translate the second rectangle to the center of the first rectangle
  translateY2 = 50*(sin(thetaXY2));//y value used to translate the second rectangle to the center of the first rectangle
  //println("x: " + translateX1);
  //println("y: " + translateY1);
  pushMatrix();//pushes the matrix for the first rectangle
  translate((width/2)+translateX1,(height/2)+translateY1);//translates the coordinate system using the translate 1 values
  rotateZ(thetaXY1);//rotates the coordinate system the required angle after compensating through translation
  rotateY(thetaZY1);
  fill(255);//makes the fill of the box white
  box(200,50,90);//creates a box with set dimenstions here
  popMatrix();//pops out of rectangle 1 coordinate system
  
  pushMatrix();//pushed matrix for the second rectangle
  translate((width/2)+lengthX1+translateX2,(height/2)+lengthY1+translateY2);//translate the second box to the end of the first box through its rotations
  rotateZ(thetaXY2);//rotates the coordinate system from given value after compensation
  box(100,50,70);//creates a box with set dimensions here
  popMatrix();//pops out of box 2 coordinate system
}

void mouseDragged(){
  if(mouseX>0 && mouseX<60)//if the mouse is within the bounds of the first slider
    value1 = mouseY;
  if(mouseX>70 && mouseX<130)//if the mouse is within the bounds of the second slider
    value2 = mouseY;
  if(mouseX>140 && mouseX<200)
    value3 = mouseY;
  if(mouseX>210 && mouseX<270)
    value4 = mouseY;
  //println("Value 1: " + value1);
  //println("Value 2: " + value2);
}
